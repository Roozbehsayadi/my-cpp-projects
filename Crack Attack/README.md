
# Crack Attack

## Installing Requirements

Graphics are implemented witl SDL. Enter this command to install it.

```shell
sudo apt install libsdl1.2-dev libsdl-gfx1.2-dev
```

## Running the Program

Simply use the Makefile to run the program. Go to the project's folder and enter ```make``` command.

## Screenshots

![Screenshot](https://www.dl.dropboxusercontent.com/s/kx3f25ocj7cglvk/Crack%20Attack.png?dl=0)
