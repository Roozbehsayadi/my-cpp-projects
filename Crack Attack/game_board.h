
#ifndef __GAME_BOARD_H
#define __GAME_BOARD_H

#include <stdlib.h>
#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <fstream>
#include <sstream>

using namespace std;

class Board{
		SDL_Surface* screen;
		int** piece;
		bool** mark;
		int x, y, dy;
		bool lost;
		int score;
	public:
		int play();
		void drawTable();
		void refresh();
		Board();
		int stopped();
		void pushUp();
		bool canMove();
		void repaint();
		void swap(int, int, int, int);
		void check();
		void gravity();
		void makeLast();
		void draw3DSquare(int, int, int);
		void moveCursor();
		void eraseCursor();
		SDL_Surface* getScreen();
		int getPiece(int, int);
		void setPiece(int, int, int);
		int getX();
		int getY();
};

#endif
