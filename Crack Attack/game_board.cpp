
#include "game_board.h"

Board::Board(){
	score = 0;
	lost = false;
	srand(time(0));
	piece = new int*[8];
	mark = new bool*[8];
	for (int i = 0; i < 8; i++){
		piece[i] = new int[13];
		mark[i] = new bool[13];
	}
	for (int i = 0; i < 6; i++)
		for (int j = 0; j < 13; j++){
			piece[i][j] = 0;
			mark[i][j] = false;
		}
	for (int i = 0; i < 6; i++)
		for (int j = 6; j < 13; j++)
			piece[i][j] = rand() % 6 + 1;
	x = 2;
	y = 7;
	dy = 1;
	screen = SDL_SetVideoMode(600, 600, 32, SDL_DOUBLEBUF);
}

void Board::refresh(){
	SDL_Flip(screen);
}

void Board::repaint(){
	 for (int i = 0; i < 6; i++)
	 	for (int j = 0; j < 13; j++){
	 		draw3DSquare(i, j, piece[i][j]);
	 	}
	 SDL_Flip(screen);
}

void Board::drawTable(){
	for (int i = 0; i < 6; i++)
		boxRGBA(screen, i * 50 - 2, 0, i * 50 + 2, 600, 0, 0, 0, 255); 
	for (int j = 0; j < 13; j++)
		boxRGBA(screen, 0, j * 50 - 2 - dy, 600, j * 50 + 2 - dy, 0, 0, 0, 255);
	SDL_Flip(screen);
}

void Board::swap(int x1, int y1, int x2, int y2){
	int k = piece[x1][y1];
	piece[x1][y1] = piece[x2][y2];
	piece[x2][y2] = k;
}

void Board::draw3DSquare(int x, int y, int color){
	int varColor = 0, width = 0;
	if (color == 0)
		boxRGBA(screen, x * 50, y * 50 - dy, (x + 1) * 50, (y + 1) * 50 - dy, 0, 0, 0, 255);
	if (color == 1)
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, 255, varColor, varColor, 255);
			varColor+= 7;
			width++;
		}
	if (color == 2)
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, varColor, 255, varColor, 255);
			varColor += 7;
			width++;
		}
	if (color == 3)
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, varColor, varColor, 255, 255);
			varColor += 7;
			width++;
		}
	if (color == 4)
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, 255, 255, varColor, 255);
			varColor += 7;
			width++;
		}
	if (color == 5)
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, 255, varColor, 255, 255);
			varColor += 7;
			width++;
		}
	if (color == 6)
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, varColor, 255, 255, 255);
			varColor += 7;
			width++;
		}
	if (color == 7){
		varColor = 255;
		for (int i = 0; i < 25; i++){
			boxRGBA(screen, x * 50 + width, y * 50 + width - dy, (x + 1) * 50 - width, (y + 1) * 50 - width - dy, 125 + varColor, 125 + varColor, 125 + varColor, 255);
			varColor += 3;
			width++;
		}
	}
}

void Board::moveCursor(){
	rectangleRGBA(screen, x * 50 - 1, y * 50 - 1 - dy, (x + 2) * 50 - 1, (y + 1) * 50 - 1 - dy, 255, 255, 255, 255);
	rectangleRGBA(screen, x * 50, y * 50 - dy, (x + 2) * 50, (y + 1) * 50 - dy, 255, 255, 255, 255);
	rectangleRGBA(screen, x * 50 + 1, y * 50 + 1 - dy, (x + 2) * 50 + 1, (y + 1) * 50 - dy + 1, 255, 255, 255, 255);
	SDL_Flip(screen);
}

void Board::eraseCursor(){
	rectangleRGBA(screen, x * 50 - 1, y * 50 - 1 - dy, (x + 2) * 50 - 1, (y + 1) * 50 - 1 - dy, 0, 0, 0, 255);
	rectangleRGBA(screen, x * 50, y * 50 - dy, (x + 2) * 50, (y + 1) * 50 - dy, 0, 0, 0, 255);
	rectangleRGBA(screen, x * 50 + 1, y * 50 + 1 - dy, (x + 2) * 50 + 1, (y + 1) * 50 + 1 - dy, 0, 0, 0, 255);
	SDL_Flip(screen);
}

SDL_Surface* Board::getScreen(){
	return screen;
}

int Board::getPiece(int x, int y){
	return piece[x][y];
}

void Board::setPiece(int x, int y, int color){
	piece[x][y] = color;
}

int Board::getX(){
	return x;
}

int Board::getY(){
	return y;
}

void Board::makeLast(){
	for (int i = 0; i < 6; i++)
		piece[i][12] = rand() % 6 + 1;
	this -> repaint();
	this -> drawTable();
	this -> moveCursor();
}

void Board::pushUp(){
	for (int i = 1; i < 13; i++)
		for (int j = 0; j < 6; j++)
			piece[j][i - 1] = piece[j][i];
	this -> makeLast();
	this -> repaint();
	this -> drawTable();
	y--;
	this -> moveCursor();
}

void Board::check(){
	bool flag = false;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 13; j++){
			if (piece[i][j] == piece[i + 1][j] && piece[i + 1][j]== piece[i + 2][j] && piece[i][j] != 0){
				mark[i][j] = true;
				mark[i + 1][j] = true;
				mark[i + 2][j] = true;
			}
		}
	for (int i = 0; i < 6; i++)
		for (int j = 0; j < 10; j++){
			if (piece[i][j] == piece[i][j + 1] && piece[i][j + 1] == piece[i][j + 2] && piece[i][j] != 0){
				mark[i][j] = true;
				mark[i][j + 1] = true;
				mark[i][j + 2] = true;
			}
		}
	for (int i = 0; i < 6; i++)
		for (int j = 0; j < 12; j++)
			if (mark[i][j] == true){
				score++;
				piece[i][j] = 0;
			}
	for (int i = 0; i < 6; i++)
		for (int j = 0; j < 12; j++)
			mark[i][j] = false;
}

void Board::gravity(){
	for (int i = 0; i < 11; i++)
		for (int j = 0; j < 6; j++)
			if (piece[j][i + 1] == 0 && piece[j][i] != 0)
			{
				piece[j][i + 1] = piece[j][i];
				piece[j][i] = 0;
				this -> repaint();
				this -> gravity();
			}
}

int Board::stopped(){
	SDL_Event event;
	int start = SDL_GetTicks(), afterStart = 0, oneSecTime = 0;
	bool oneSec = false;
	while (1){
		if (!oneSec)
			afterStart = SDL_GetTicks() - start;
		if (afterStart >= 7000){
			oneSec = true;
			afterStart = 7000;
		}
		if (oneSec)
			oneSecTime = SDL_GetTicks() - start - afterStart;
		if (oneSecTime >= 1000){
			cout << "YOU LOST!" << endl << "YOUR SCORE IS " << score << endl;
			lost = true;
			return 0;
		}
		if (this -> canMove())
			return 0;
		if (SDL_PollEvent(&event)){
			if (event.type == SDL_QUIT){
				SDL_Quit();
				return 0;
			}
			if (event.type == SDL_KEYDOWN){
				this -> eraseCursor();
				if (event.key.keysym.sym == SDLK_LEFT && x > 0)
					x--;
				if (event.key.keysym.sym == SDLK_UP && y > 0)
					y--;
				if (event.key.keysym.sym == SDLK_RIGHT && x < 4)
					x++;
				if (event.key.keysym.sym == SDLK_DOWN && y < 11)
					y++;
				this -> moveCursor();
				if (event.key.keysym.sym == SDLK_SPACE){
					this -> swap(x, y, x + 1, y);
					this -> check();
					this -> gravity();
					this -> repaint();
					this -> drawTable();
					this -> moveCursor();
				}
			}
		}
	}
}

bool Board::canMove(){
	bool flag = false;
	for (int i = 0; i < 6; i++){
		if (piece[i][0] != 0)
			flag = true;
	}
	if (flag)
		return false;
	return true;
}

int Board::play(){
	SDL_Event event;
	int passedTime = 0, timeBack = 0, dif = 0, difSum = 0, boxWidth = 50;
	this -> repaint();
	this -> drawTable();
	this -> moveCursor();
	while (1){
		passedTime = SDL_GetTicks();
		if (this -> canMove()){
			if (passedTime % 100 == 0){
				dy += 1;
				this -> check();
				this -> gravity();
				this -> repaint();
				this -> drawTable();
				this -> moveCursor();
			}
			if (dy >= boxWidth){
				dy = 0;
				this -> pushUp();
				this -> check();
				this -> gravity();
				this -> repaint();
				this -> drawTable();
				this -> moveCursor();
			}
		}
		if (!this -> canMove())
			this -> stopped();
		if (lost){
			SDL_Quit();
			return false;
		}
		while (SDL_PollEvent(&event)){
			if (event.type == SDL_QUIT)
				return 0;
			if (event.type == SDL_KEYDOWN){
				this -> eraseCursor();
				if (event.key.keysym.sym == SDLK_LEFT && x > 0)
					x--;
				if (event.key.keysym.sym == SDLK_UP && y > 0)
					y--;
				if (event.key.keysym.sym == SDLK_RIGHT && x < 4)
					x++;
				if (event.key.keysym.sym == SDLK_DOWN && y < 11)
					y++;
				this -> moveCursor();
				if (event.key.keysym.sym == SDLK_SPACE){
					this -> swap(x, y, x + 1, y);
					this -> check();
					this -> gravity();
					this -> repaint();
					this -> drawTable();
					this -> moveCursor();
				}
				if (event.key.keysym.sym == SDLK_RETURN && (piece[0][1] == 0 || piece[0][2] == 0 || piece[0][3] == 0 || piece[0][4] == 0 || piece[0][5] == 0)){
					this -> pushUp();
					this -> check();
					this -> gravity();
					this -> repaint();
					this -> drawTable();
					this -> moveCursor();
				}
			}
		}
	}
}
