
#include <iostream>
#include <SDL/SDL.h>
#include "game_board.h"

using namespace std;

int main(){
	SDL_Init(SDL_INIT_EVERYTHING);
	Board* board = new Board();
	board -> play();
	delete board;
	return 0;
}
