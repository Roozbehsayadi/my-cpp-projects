
#include "voiceMessage.h"

VoiceMessage::VoiceMessage() {
	this->setType( Message::VOICE );
}

VoiceMessage::VoiceMessage( long long callLength ) {
	this->setCallLength( callLength );
}

double VoiceMessage::getCost( vector <City> &cities, string start, string destination ) { 
//	return ( pow( this->getLength(), 2 ) * PathStuff::getDistance( this->getStartCity(), this->getDestCity() ) / 10 );
	return ( pow( this->getLength(), 2 ) * MessageDistance( cities, this->getStartCity(), this->getDestCity() ) / 10 );
}

