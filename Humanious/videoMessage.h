
#ifndef VIDEOMESSAGE_H
#define VIDEOMESSAGE_H

#include "telephoneMessage.h"

//#include "pathStuff.h"

class VideoMessage : public TelephoneMessage {

	private:
		int videoWidth, videoHeight;

	public:

		VideoMessage( long long, int, int );
		
		virtual double getCost( vector <City>&, string, string );

		void setVideoWidth( int );
		void setVideoHeight( int );

		int getVideoWidth() const;
		int getVideoHeight() const;

};

#endif

