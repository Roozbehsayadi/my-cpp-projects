
#ifndef __SECURITYLEVEL_H
#define __SECURITYLEVEL_H

#include <string>

using namespace std;

class SecurityLevel {

	private:
		string *type;

	public:
		
		SecurityLevel();
		~SecurityLevel();

		virtual double getLength( long long ) const = 0;

		string getType() const;

		void setType( string );


};

#endif

