
#ifndef __SEMISAFETEXT_H
#define __SEMISAFETEXT_H

#include <math.h>

#include "securityLevel.h"

class SemisafeText : public SecurityLevel {

	public:

		SemisafeText();

		virtual double getLength( long long ) const;

};

#endif

