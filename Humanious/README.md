# Humanious

## Project Description

Project's description is available in **Project Description.pdf** file.

## Running the Program

You can use the makefile. Just open the folder with your command prompt and type ```make```.

## Code

As you may understand from the description, this is a project involving many classes and inheritances.

The main class is Humanious. It handles inputs and generates appropriate outputs.

Important classes and their childs:

* **SecurityLevel**: It has a *type* field (which can be *safe*, *unsafe* or *semi-safe*) and a *getLength* method. The algorithm of calculating the length is changed based on the type, so it's a virtual function.

 It is implemented in *SafeTelephone*, *SafeText*, *SemisafeTelephone*, *SemisafeText*, *UnsafeTelephone* and *UnsafeText*.

* **CommunicationSpeed**: This class has a *type* field (which can be *fast* or *slow*) and a *multiplier* method. For text messages, you can choose whether you want to send it fast or slow. in fast mode, a coefficient is multiplied by the total cost of message transition. The value of this coefficient it calculated in *FastText* class.

In other message modes (voice and video), speed is undefined and the coefficient is 1. It is calculated in *Normal* class.

* **Message**: It's the base class for all three types of messages. Three main methods of this class are *getCost*, *getLength* and *getActualLength*. *getActualLength* returns the length of the message, while *getLength* returns the length of the message based on its security level.

*TextMessage* and *TelephoneMessage* inherit from this class. Also *VoiceMessage* and *VideoMessage* inherit from *TelephoneMessage* class.

Other classes:

* **City**: Used to store the *city* entity in the program. Every city has a *name*, *types of connections* it can make, and some adjacent cities. Also each road between cities has a length (which is stored in *mojaaver* field).

* **PathStuff**: Some static fields to do the calculations for paths. It includes

  * checking if two cities are connected or not
  
  * and getting the distance between two cities.
