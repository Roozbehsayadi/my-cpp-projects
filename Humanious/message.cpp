
#include "message.h"
/*
Message::Message( string start, string dest ) {

//	this->securityLevel = new UnsafeSecurityLevel();
//	this->communicationSpeed = new SlowCommunicationSpeed();

//	Message();

	this->setStartCity( start );
	this->setDestCity( dest );

}*/

Message::~Message() {
	delete this->securityLevel;
	delete this->communicationSpeed;
}

int Message::getType() const { 
	return this->type;
}

const CommunicationSpeed &Message::getCommunicationSpeed() const {
	return *(this->communicationSpeed);
}

const string &Message::getStartCity() const {
	return this->startCity;
}

const string &Message::getDestCity() const {
	return this->destCity;
}

void Message::printDetails( vector <City> &cities, const string &name1, const string &name2 ) { 
//	cout << "HERE! " << name1 << ' ' << name2 << ' ' << this->getTypeDetail( this->getType() ) << endl;
//	if ( PathStuff::isPathAvailable( cities, name1, name2, this->typeDetail[ this->getType() ] ) )
//	cout << "TYPE: " << this->getType() << endl;
	string s = "HARCHI";
	if ( !MessageIsPathAvailable( cities, name1, name2, s ) )
		cout << "No path found." << endl;
	else if ( MessageIsPathAvailable( cities, name1, name2, this->getTypeDetail( this->getType() ) ) )
		printf( "Distance: %lld, Price: %.2lf\n", MessageDistance( cities, name1, name2 ), this->getCost( cities, name1, name2 ) );
	else 
		cout << this->getTypeDetail( this->getType() ) << " is not available in this path." << endl;
}

void Message::setCommunicationSpeed( const CommunicationSpeed &communicationSpeed ) {
	if ( this->communicationSpeed != NULL )
		delete this->communicationSpeed;
	if ( communicationSpeed.getType() == "FastText" )
		this->communicationSpeed = new FastText;
	else if ( communicationSpeed.getType() == "Normal" )
		this->communicationSpeed = new Normal;
/*	else{
		cout << "CommunicationSpeedType not recognized... Exiting!" << endl;
		exit( 0 );
	}*/
}

void Message::setType( int messageType ) {
	this->type = messageType;
}

void Message::setStartCity( string startCity ){
	this->startCity = startCity;
}

void Message::setDestCity( string destCity ) {
	this->destCity = destCity;
}

void Message::setSecurityLevel( const SecurityLevel& securityLevel ) {
	if ( this->securityLevel != NULL )
		delete this->securityLevel;
//	cout << "SECURITY LEVEL TYPE: " << securityLevel.getType() << endl;
//	this->securityLevel = new SecurityLevel( securityLevel );
	if ( securityLevel.getType() == "UnsafeText" )
		this->securityLevel = new UnsafeText;
	else if ( securityLevel.getType() == "UnsafeTelephone" )
		this->securityLevel = new UnsafeTelephone;
	else if ( securityLevel.getType() == "SemisafeText" )
		this->securityLevel = new SemisafeText;
	else if ( securityLevel.getType() == "SemisafeTelephone" )
		this->securityLevel = new SemisafeTelephone;
	else if ( securityLevel.getType() == "SafeText" )
		this->securityLevel = new SafeText;
	else if ( securityLevel.getType() == "SafeTelephone" )
		this->securityLevel = new SafeTelephone;
/*	else {
		cout << "SecurityLevelType not recognized... Exiting!" << endl;
		exit( 0 );
	}*/
}

const SecurityLevel &Message::getSecurityLevel() const {
	return *(this->securityLevel);
}

