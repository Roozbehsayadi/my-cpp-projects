
#ifndef __MESSAGE_H
#define __MESSAGE_H

#include <string>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <map>
#include <queue>

#include "city.h"
//#include "pathStuff.h"

#include "communicationSpeed.h"
#include "fastText.h"
#include "normal.h"

#include "securityLevel.h"
#include "unsafeText.h"
#include "unsafeTelephone.h"
#include "semisafeText.h"
#include "semisafeTelephone.h"
#include "safeText.h"
#include "safeTelephone.h"

using namespace std;

inline City &MessageFindCity( vector <City> &cities, const string &name ) {
	for ( int i = 0; i < cities.size(); i++ )
		if ( cities[ i ].getName() == name )
			return cities[ i ];
}

inline bool MessageDFS( vector <City> &cities, map <string, bool> &mark, string currentName, string destinationName, string type ) {
	if ( currentName == destinationName )
		return true;
	mark[ currentName ] = true;
	map <City, int> temp = MessageFindCity( cities, currentName ).getMojaavers();
	for ( map <City, int>::iterator it = temp.begin(); it != temp.end(); it++ )
		if ( mark[ it->first.getName() ] == false )
			if ( MessageFindCity( cities, it->first.getName() ).hasConnectionType( type ) )
				if ( MessageDFS( cities, mark, it->first.getName(), destinationName, type ) )
					return true;
	return false;
}

inline bool MessageIsPathAvailable( vector <City> &cities, const string &name1, const string &name2, string type ) {
	if ( ! MessageFindCity( cities, name1 ).hasConnectionType( type ) )
		return false;
	map <string, bool> mark;
	for ( int i = 0; i < cities.size(); i++ )
		mark[ cities.at( i ).getName() ] = false;
	return MessageDFS( cities, mark, name1, name2, type );
}

inline long long &MessageDistance( vector <City> &cities, string start, string destination ) {

	map <string, long long> currentCost;
	for ( int i = 0; i < cities.size(); i++ )
		currentCost[ cities[i].getName() ] = ( long long ) ( pow( 2, 60 ) );
	map <string, bool> mark;
	for ( int i = 0; i < cities.size(); i++ )
		mark[ cities[i].getName() ] = false;
	queue <string> saf;
	saf.push( start );
	currentCost[ start ] = 0;

	while ( !saf.empty() ){
		int size = saf.size();
		for ( int i = 0; i < size; i++ ){
			string x = saf.front();
			for ( int j = 0; j < MessageFindCity( cities, x ).mojaaverCount(); j++ ) {
//				string y = MessageFindCity( cities, x ).getMojaaver( j ).getName();
				string y = MessageFindCity( cities, x ).getMojaaverName( j );
				if ( mark[ y ] == false ){
					if ( MessageFindCity( cities, x ).getMojaaverByName( y ) + currentCost[ x ] < currentCost[ y ] ){
						currentCost[ y ] = MessageFindCity( cities, x ).getMojaaverByName( y ) + currentCost[ x ];
					}
					saf.push( y );
				}
			}
			mark[ x ] = true;
			saf.pop();
		}
	}
	return currentCost[ destination ];

}

class Message {

	private:
		CommunicationSpeed *communicationSpeed = NULL;
		SecurityLevel *securityLevel = NULL;

		string startCity, destCity;

		int type;

	public:

		const static int MESSAGE = 0, VOICE = 1, VIDEO = 2;
		const static string typeDetail[3];

//		Message( string, string );
//		Message();
		~Message();

		virtual double getCost( vector <City> &, string, string ) = 0;
		virtual double getLength() const = 0;
		virtual long long getActualLength() const = 0;

		void printDetails( vector <City> &, const string&, const string& );
		string getTypeDetail( int type ) const {
			if ( type == Message::MESSAGE )
				return "TEXT";
			else if ( type == Message::VOICE )
				return "VOICE";
			else if ( type == Message::VIDEO )
				return "VIDEO";
		}
		
		int getType() const;
		const CommunicationSpeed &getCommunicationSpeed() const;
		const string &getStartCity() const;
		const string &getDestCity() const;
		const SecurityLevel& getSecurityLevel() const;

		void setType( int );
		void setCommunicationSpeed( const CommunicationSpeed& );
		void setStartCity( string );
		void setDestCity( string );
		void setSecurityLevel( const SecurityLevel& );

};

//const string typeDetail[3] = { "MESSAGE", "VOICE", "VIDEO" };

#endif

