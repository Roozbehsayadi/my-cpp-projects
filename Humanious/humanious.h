
#ifndef __HUMANIOUS_H
#define __HUMANIOUS_H

#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

#include "city.h"
//#include "pathStuff.h"

//#include "message.h"
#include "textMessage.h"
#include "telephoneMessage.h"
#include "voiceMessage.h"
#include "videoMessage.h"
/*
#include "securityLevel.h"
#include "unsafeText.h"
#include "unsafeTelephone.h"
#include "semisafeText.h"
#include "semisafeTelephone.h"
#include "safeText.h"
#include "safeTelephone.h"

#include "communicationSpeed.h"
#include "fastText.h"
#include "normal.h"
*/

class Message;
/*class TextMessage;
class TelephoneMessage;
class VoiceMessage;
class VideoMessage;
*/
class SecurityLevel;
class UnsafeText;
class UnsafeTelephone;
class SemisafeText;
class SemisafeTelephone;
class SafeText;
class SafeTelephone;

class CommunicationSpeed;
class FastText;
class Normal;

class PathStuff;

using namespace std;

class Humanious {

	public:

		Humanious();
		
		void run();

		static City findCity( const vector <City> &, string );

	private:

		const static int TEXT = 0, VOICE = 1, VIDEO = 2;
		const static int SAFE = 0, SEMISAFE = 1, UNSAFE = 2;
		const static int FAST = 0, SLOW = 1;

		vector <City> cities;

		City& findCity( string );
		void makeMessage( Message*&, string );

		bool setSecurityLevel( int&, string );
		bool setCommunicationSpeed( int&, string );
		void makeSecurityLevel( SecurityLevel*&, int, string );
		void makeCommunicationSpeed( CommunicationSpeed*&, int );

		void getInitialStuff();
		void getCityProperties();
		void getDists();

};

#endif 

