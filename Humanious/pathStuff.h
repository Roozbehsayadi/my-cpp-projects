
#ifndef __PATHSTUFF_H
#define __PATHSTUFF_H

#include <string>

#include "humanious.h"
#include "city.h"

using namespace std;

class PathStuff{

        private:

                static bool DFS( const vector <City>&, map <string,bool>&, string, string, string );

        public:

                static bool isPathAvailable( const vector <City>&, const string&, const string&, const string& );

		static long getDistance( string, string );

};

#endif
