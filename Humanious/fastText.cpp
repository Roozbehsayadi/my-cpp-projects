
#include "fastText.h"

FastText::FastText() {
	this->setType( "FastText" );
}

int FastText::multiplier( string message ) const {
//	std::cout << message.length() << "-------" << (double) (message.length() / 140 ) << std::endl;
	return ceil( ( ( (double) (message.length()) / 140 ) ) );
}

