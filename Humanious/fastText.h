
#ifndef __FASTTEXT_H
#define __FASTTEXT_H

#include <iostream>
#include <math.h>

#include "communicationSpeed.h"

class FastText : public CommunicationSpeed { 

	public:

		FastText();
		
		virtual int multiplier( string ) const;

};

#endif

