
#include "securityLevel.h"

SecurityLevel::SecurityLevel() {
	this->type = new string;
}

SecurityLevel::~SecurityLevel() {
	delete this->type;
}

void SecurityLevel::setType( string type ) {
	*( this->type ) = type;
}

string SecurityLevel::getType() const { 
	return *(this->type );
}

