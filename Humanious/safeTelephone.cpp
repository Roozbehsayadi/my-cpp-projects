
#include "safeTelephone.h"

SafeTelephone::SafeTelephone() {
	this->setType( "SafeTelephone" );
}

double SafeTelephone::getLength( long long length ) const {
	return ( log( length ) * length );
}

