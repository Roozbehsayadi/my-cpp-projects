
#ifndef TELEPHONEMESSAGE_H
#define TELEPHONEMESSAGE_H

#include "message.h"

class TelephoneMessage : public Message { 

	private:

		long long callLength;

	public:

		virtual double getCost( vector <City> &, string, string ) = 0;

		virtual long long getActualLength() const;

		virtual double getLength() const;

		void setCallLength( long long );

};

#endif

