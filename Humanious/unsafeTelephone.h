
#ifndef __UNSAFETELEPHONE_H
#define __UNSAFETELEPHONE_H

#include "securityLevel.h"

class UnsafeTelephone : public SecurityLevel {

	public:

		UnsafeTelephone();

		virtual double getLength( long long ) const;

};

#endif

