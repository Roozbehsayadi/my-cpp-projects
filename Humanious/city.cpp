
#include "city.h"

City::City() {
	this->name = "";
	this->connectionTypes.resize( 0 );
	this->mojaaverName.resize( 0 );
}

City::City( string name ) {
	this->name = name;
	this->connectionTypes.resize( 0 );
	this->mojaaverName.resize( 0 );
}

void City::addConnectionType( const string &connectionType ) {
//	cout << "HERE WE ARE, in the ADDCONNECTIONTYPE METHOD! Passed argument is " << connectionType << endl;
//	cout << connectionTypes.size() << endl;
/*	if ( this->connectionTypes.size() != 0 )
		cout << this->connectionTypes.at( 0 ) << endl;*/
	this->connectionTypes.push_back( connectionType );
//	cout << this->connectionTypes.at( 0 ) << endl;
//	cout << this->connectionTypes[0] << endl;
//	cout << "DONE!" << endl;
}

bool City::hasConnectionType( const string &connectionType ) {
	if ( connectionType == "HARCHI" )
		return true;
	return ( find( this->connectionTypes.begin(), this->connectionTypes.end(), connectionType ) != this->connectionTypes.end() );
}

void City::addMojaaver( string name, int distance ) {
	City temp( name );
	this->mojaaver[ temp ] = distance;
	this->mojaaverString[ name ] = distance;
	this->mojaaverName.push_back( name );
}

const string &City::getMojaaverName( int index ) const {
//	return *( this->mojaaver.begin() + index );
	return this->mojaaverName[ index ];
}

int City::getMojaaverByName( string name )  {
	return this->mojaaverString[ name ];
}

int City::mojaaverCount() const {
	return this->mojaaver.size();
}

bool City::operator<( const City &city ) const {
	return ( this->getName() < city.getName() );
}

const map <City, int> &City::getMojaavers() const {
	return this->mojaaver;
}

string City::getName() const {
	return this->name;
}

void City::setName( string name ) {
	this->name = name;
}

