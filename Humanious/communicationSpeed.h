
#ifndef __COMMUNICATIONSPEED_H
#define __COMMUNICATIONSPEED_H

#include <string>

using namespace std;

class CommunicationSpeed { 

	private:
		string *type;

	public:
		CommunicationSpeed();
		~CommunicationSpeed();

		virtual int multiplier( string ) const = 0;

		void setType( string );

		string getType() const;

};

#endif

