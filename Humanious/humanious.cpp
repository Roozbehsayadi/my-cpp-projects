
#include "humanious.h"

Humanious::Humanious() {
	this->cities.resize( 0 );
}

void Humanious::run() {

	this->getInitialStuff();
	
//	cout << "NAMES: " << this->cities[ 0 ].getName() << ' ' << this->cities[ 1 ].getName() << endl;
/*	for ( int i = 0; i < this->cities.size(); i++ )
		cout << this->cities[ i ].getName() << endl;*/
/*	cout << this->cities[ 0 ].getName() << ' ' << this->cities[ 4 ].getName() << endl;
	cout << "OUTPUT: " << PathStuff::isPathAvailable( this->cities, this->cities[ 0 ], this->cities[ 4 ] ) << endl;*/

	SecurityLevel *securityLevel;
	CommunicationSpeed *communicationSpeed;
	int security = UNSAFE, speed = SLOW;

	while ( true ) {
		string first;
		cin >> first;
//		cout << "FIRST: " << first << endl;
		if ( this->setSecurityLevel( security, first ) || this->setCommunicationSpeed( speed, first ) ){
//			cout << "CONTINUEING!" << endl;
			continue;
		}
		string second;
		cin >> second;
		string type;
		cin >> type;
		if ( type == "TEXT" || type == "VOICE_CALL" || type == "VIDEO_CALL" ){
			this->makeSecurityLevel( securityLevel, security, type );
			this->makeCommunicationSpeed( communicationSpeed, speed );
//			cout << "SecurityLevel Address: " << securityLevel << endl;
//			cout << "DONE MAKESECURITY LEVEL! " << securityLevel->getType() << endl;
		}
//		cout << "SECURITY LEVEL: " << securityLevel->getType() << endl;
//		communicationSpeed = new Normal;
//		cout << "Normal communication speed created!" << endl;

		Message* message;
//		cout << "NOW... Making a new message with type " << type << "!" << endl;
		this->makeMessage( message, type );
//		cout << message->getType() << endl;
		message->setSecurityLevel( *securityLevel );
		message->setCommunicationSpeed( *communicationSpeed );
		message->setStartCity( first );
		message->setDestCity( second );
		message->printDetails( this->cities, first, second );
		delete message;
	}

}

void Humanious::makeMessage( Message *&message, string type ) {

	if ( type == "TEXT" ) {
		string messageText;
		cin >> messageText;
		message = new TextMessage( messageText );
	}
	else if ( type == "VOICE_CALL" ){
		long long callLength;
		cin >> callLength;
		message = new VoiceMessage( callLength );
	}
	else if ( type == "VIDEO_CALL" ) {
		long long callLength;
		int videoWidth, videoHeight;
		cin >> callLength;
		scanf( "%d*%d", &videoWidth, &videoHeight );
		message = new VideoMessage( callLength, videoWidth, videoHeight );
	}

}

void Humanious::makeSecurityLevel( SecurityLevel*& securityLevel, int security, string type ) {
	if ( security == Humanious::SAFE ){
		if ( type == "TEXT" ){
			securityLevel = new SafeText();
//			cout << "securityLevel Address: " << securityLevel << endl;
//			cout << "CREATING SAFETEXT!" << endl;
//			cout << "SEFSEFSEF   " << securityLevel->getType() << endl;
		}
		else if ( type == "VOICE_CALL" || type == "VIDEO_CALL" ){
			securityLevel = new SafeTelephone();
//			cout << "CREATING SAFETELEPHONE!" << endl;
		}
	}
	if ( security == Humanious::SEMISAFE ) {
		if ( type == "TEXT" ){
			securityLevel = new SemisafeText();
//			cout << "CREATING SEMISAFETEXT!" << endl;
		}
		else if ( type == "VOICE_CALL" || type == "VIDEO_CALL" ){
			securityLevel = new SemisafeTelephone();
//			cout << "CREATING SEMISAFETELEPHONE!" << endl;
		}
	}
	if ( security == Humanious::UNSAFE ) {
		if ( type == "TEXT" ){
			securityLevel = new UnsafeText();
//			cout << "CREATING UNSAFETEXT!" << endl;
		}
		else if ( type == "VOICE_CALL" || type == "VIDEO_CALL" ){
			securityLevel = new UnsafeTelephone();
//			cout << "CREATING UNSAFETELEPHONE!" << endl;
		}
	}
}

void Humanious::makeCommunicationSpeed( CommunicationSpeed*& communicationSpeed, int speed ) {
	if ( speed == Humanious::FAST )
		communicationSpeed = new FastText;
	else if ( speed == Humanious::SLOW )
		communicationSpeed = new Normal;
}

bool Humanious::setCommunicationSpeed( int &communicationSpeed, string s ) {
	if ( s == "Fast" ){
		communicationSpeed = Humanious::FAST;
		return true;
	}
	else if ( s == "Slow" ){
		communicationSpeed = Humanious::SLOW;
		return true;
	}
	return false;
}

bool Humanious::setSecurityLevel( int &securityLevel, string s ) {
	if ( s == "Safe" ){
//		cout << "SAFE DETECTED!" << endl;
		securityLevel = Humanious::SAFE;
		return true;
	}
	else if ( s == "Semi-Safe" ){
//		cout << "SEMISAFE DETECTED!" << endl;
		securityLevel = Humanious::SEMISAFE;
		return true;
	}
	else if ( s == "Unsafe" ){
//		cout << "UNSAFE DETECTED!" << endl;
		securityLevel = Humanious::UNSAFE;
		return true;
	}
	return false;
}

void Humanious::getInitialStuff() {
	this->getCityProperties();
//	cout << this->cities.size() << endl;
	this->getDists();
}

void Humanious::getCityProperties() {
	while ( true ) {
		string cityLabel;
		cin >> cityLabel;
		if ( cityLabel == "#" )
			break;
		City *currentCity = new City( cityLabel );
		string wholeLine;
		getline( cin, wholeLine );
//		cout << "WHOLE LINE: " << wholeLine[1] << endl;
//		cout << "HERE WE ARE... BEFORE THE FOR!" << endl;
		for ( int i = 0; i < wholeLine.length(); i++ ){
//			cout << "HERE WE ARE!" << endl;
			string currentType;
			currentType.resize( 0 );
			if ( wholeLine[ i ] != ' ' ){
//				cout << wholeLine[ i ] << endl;
				while ( wholeLine[i] != ' ' && i < wholeLine.length() ){
//					cout << "Current: " << wholeLine[ i ] << endl;
					currentType += wholeLine[ i ];
					i++;
				}
//				cout << "THIS: " << currentType << endl;
//				cout << currentType << endl;
//				cout << "CONNECTION TYPE: \"" << currentType << "\"!" << endl;;
//				cout << "HERE! " << currentType << endl;
//				cout << currentType << endl;
//				cout << currentCity->mojaaverCount() << endl;
//				cout << currentCity->getName() << endl;
				currentCity->addConnectionType( currentType );
//				cout << "HERE2!" << endl;
			}
			else continue;
		}
//		cout << "CITY NAME: " << currentCity->getName() << endl;
		this->cities.push_back( *currentCity );
		delete currentCity;
	}
}

void Humanious::getDists() {

	while ( true ) {
		string start;
		cin >> start;
		if ( start == "$" )
			break;
		string dest;
		int distance;
		cin >> dest >> distance;
		this->findCity( start ).addMojaaver( dest, distance );
		this->findCity( dest ).addMojaaver( start, distance );
	}

}

City &Humanious::findCity( string label ) {
	for ( int i = 0; i < this->cities.size(); i++ )
		if ( this->cities.at( i ).getName() == label )
			return this->cities.at( i );
//	return Humanious::findCity( this->cities, label );
}

City Humanious::findCity( const vector <City> &cities, string label ) {
	for ( int i = 0; i < cities.size(); i++ )
		if ( cities.at( i ).getName() == label ) 
			return cities.at( i );
}

