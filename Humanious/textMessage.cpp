
#include "textMessage.h"

TextMessage::TextMessage() {
	this->setType( Message::MESSAGE );
}

TextMessage::TextMessage( string s ) {
	this->setType( MESSAGE );
	this->setMessage( s.substr( 1, s.length() - 2 ) );
}

double TextMessage::getCost( vector <City> &cities, string start, string destination ) { 
/*	cout << "TextMwssage properties: " << endl;
	cout << "Message: " << this->getMessage() << endl;
	cout << "Message length: " << this->getMessage().length() << endl;*/
//	cout << "Multiplier: " << this->getCommunicationSpeed().multiplier( this->getMessage() ) << endl;
	return ( 40 * this->getLength() * this->getCommunicationSpeed().multiplier( this->getMessage() ) );
}

long long TextMessage::getActualLength() const {
	return this->getMessage().size();
}

string TextMessage::getMessage() const {
	return this->message;
}

void TextMessage::setMessage( string message ) {
	this->message = message;
}

double TextMessage::getLength() const {
	return this->getSecurityLevel().getLength( this->getActualLength() );
}

