
#include "pathStuff.h"

bool PathStuff::isPathAvailable( const vector <City> &cities, const string &name1, const string &name2, const string &type ){
	if ( !Humanious::findCity( cities, name1 ).hasConnectionType( type ) )
		return false;
	map <string, bool> mark;
	for ( int i = 0; i < cities.size(); i++ )
		mark[ cities.at( i ).getName() ] = false;
/*	for ( map<string, bool>::iterator it = mark.begin(); it != mark.end(); it++ )
		cout << it->first << "->" << it->second << endl;*/
	return PathStuff::DFS( cities, mark, name1, name2, type );
}

long PathStuff::getDistance( string s1, string s2 ){
	return 10;
}

bool PathStuff::DFS( const vector <City> &cities, map <string, bool> &mark, string currentName, string destinationName, string type ) {
//	cout << currentName << ' ' << destinationName << endl;
	if ( currentName == destinationName )
		return true;
	mark[ currentName ] = true;
/*	City currentCity = Humanious::findCity( cities, currentName );
	map <City, int> temp = currentCity.getMojaavers();*/
	map <City, int> temp = Humanious::findCity( cities, currentName ).getMojaavers();
	for ( map <City, int>::iterator it = temp.begin(); it != temp.end(); it++ ) 
		if ( mark[ it->first.getName() ] == false )
			if ( Humanious::findCity( cities, it->first.getName() ).hasConnectionType( type ) )
				if ( PathStuff::DFS( cities, mark, it->first.getName(), destinationName, type ) )
					return true;
	return false;
}

