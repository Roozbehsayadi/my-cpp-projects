
#include "telephoneMessage.h"

void TelephoneMessage::setCallLength( long long callLength ){
	this->callLength = callLength;
}

long long TelephoneMessage::getActualLength() const {
	return this->callLength;
}

double TelephoneMessage::getLength() const {
	return this->getSecurityLevel().getLength( this->getActualLength() );
}

