
#ifndef __SEMISAFETELEPHONE_H
#define __SEMISAFETELEPHONE_H

#include <math.h>

#include "securityLevel.h"

class SemisafeTelephone : public SecurityLevel { 

	public:

		SemisafeTelephone();
		
		virtual double getLength( long long ) const;

};

#endif

