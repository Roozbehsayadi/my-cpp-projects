
#include "safeText.h"

SafeText::SafeText() {
	cout << "IN SAFETEXT CONSTRUCTOR!" << endl;
	this->setType( "SafeText" );
}

double SafeText::getLength( long long length ) const { 
	return ( log( pow( length, 2 ) ) * 200 + 30 );
}

