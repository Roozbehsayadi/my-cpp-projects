
#ifndef __SAFETELEPHONE_H
#define __SAFETELEPHONE_H

#include <math.h>

#include "securityLevel.h"

class SafeTelephone : public SecurityLevel { 

	public:

		SafeTelephone();

		virtual double getLength( long long ) const;

};

#endif

