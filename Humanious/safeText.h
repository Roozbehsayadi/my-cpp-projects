
#ifndef __SAFETEXT_H
#define __SAFETEXT_H

#include <iostream>
#include <math.h>

#include "securityLevel.h"

using namespace std;

class SafeText : public SecurityLevel {

	public:

		SafeText();

		virtual double getLength( long long ) const;

};

#endif

