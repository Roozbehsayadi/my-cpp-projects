
#include "videoMessage.h"

VideoMessage::VideoMessage( long long callLength, int videoWidth, int videoHeight ) {
	this->setType( Message::VIDEO );
	this->setCallLength( callLength );
	this->setVideoWidth( videoWidth );
	this->setVideoHeight( videoHeight );
}

double VideoMessage::getCost( vector <City> &cities, string start, string destination ) {
//	return ( this->getLength() * log10( this->getVideoWidth() * this->getVideoHeight() ) * PathStuff::getDistance( this->getStartCity(), this->getDestCity() ) / 10 );
	return ( this->getLength() * log( this->getVideoWidth() * this->getVideoHeight() ) * MessageDistance( cities, this->getStartCity(), this->getDestCity() ) / 10 );
}

void VideoMessage::setVideoWidth( int videoWidth ){
	this->videoWidth = videoWidth;
}

void VideoMessage::setVideoHeight( int videoHeight ){
	this->videoHeight = videoHeight;
}

int VideoMessage::getVideoWidth() const {
	return this->videoWidth;
}

int VideoMessage::getVideoHeight() const {
	return this->videoHeight;
}

