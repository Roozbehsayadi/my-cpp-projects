
#include "semisafeText.h"

SemisafeText::SemisafeText() {
	this->setType( "SemisafeText" );
}

double SemisafeText::getLength( long long length ) const {
	return ( log( length ) * 100 + 20 );
}

