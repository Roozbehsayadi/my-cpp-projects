
#ifndef __CITY_H
#define __CITY_H

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>

using namespace std;

class City {

	private:

		string name;
		vector <string> connectionTypes;
		map <City, int> mojaaver;
		vector <string> mojaaverName;
		map <string, int> mojaaverString;

	public:

		City();
		City( string );

		void addConnectionType( const string& );
		void addMojaaver( string, int );

		const string &getMojaaverName( int ) const;
		int getMojaaverByName( string ) ;

		bool hasConnectionType( const string& );

		int mojaaverCount() const;

		bool operator<( const City & ) const;

		const map <City, int> &getMojaavers() const;
		string getName() const;

		void setName( string );

};

#endif

