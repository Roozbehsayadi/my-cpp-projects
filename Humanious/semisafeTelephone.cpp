
#include "semisafeTelephone.h"

SemisafeTelephone::SemisafeTelephone() {
	this->setType( "SemisafeTelephone" );
}

double SemisafeTelephone::getLength( long long length ) const { 
	return ( M_PI * M_PI * length );
}

