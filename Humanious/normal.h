
#ifndef __NORMAL_H
#define __NORMAL_H

#include "communicationSpeed.h"

class Normal : public CommunicationSpeed { 

	public:

		Normal();

		virtual int multiplier( string ) const;

};

#endif

