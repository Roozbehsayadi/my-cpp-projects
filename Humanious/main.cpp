
#include <iostream>

#include "humanious.h"

using namespace std;

int main () {

	Humanious *humanious = new Humanious();
	humanious->run();
	delete humanious;

	return 0;

}
