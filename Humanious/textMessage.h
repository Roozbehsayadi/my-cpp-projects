
#ifndef __TEXTMESSAGE_H
#define __TEXTMESSAGE_H

#include <string>

#include "message.h"

using namespace std;

class TextMessage : public Message {

	private:
		string message;

	public:
		TextMessage();
		TextMessage( string );

		virtual double getCost( vector <City>&, string, string ) ;
		virtual double getLength() const;
		virtual long long getActualLength() const;

		string getMessage() const;

		void setMessage( string );

};

#endif

