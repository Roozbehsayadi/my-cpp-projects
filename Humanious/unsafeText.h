
#ifndef __UNSAFETEXT_H
#define __UNSAFETEXT_H

#include "securityLevel.h"

class UnsafeText : public SecurityLevel {

	public:

		UnsafeText();

		virtual double getLength( long long ) const;

};

#endif

