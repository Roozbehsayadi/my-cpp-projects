
#ifndef __VOICEMESSAGE_H
#define __VOICEMESSAGE_H

#include "telephoneMessage.h"

//#include "pathStuff.h"

class VoiceMessage : public TelephoneMessage{

	public:

		VoiceMessage();

		VoiceMessage( long long );

		virtual double getCost( vector <City> &, string, string );
		
};

#endif

