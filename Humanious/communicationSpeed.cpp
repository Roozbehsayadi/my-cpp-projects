
#include "communicationSpeed.h"

CommunicationSpeed::CommunicationSpeed () {
	this->type = new string;
}

CommunicationSpeed::~CommunicationSpeed() {
	delete this->type;
}

void CommunicationSpeed::setType( string type ) {
	*(this->type) = type;
}

string CommunicationSpeed::getType() const { 
	return *(this->type);
}

