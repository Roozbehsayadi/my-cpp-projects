
#include "oval.h"

void Oval::draw( SDL_Surface *screen, int x, int y, int x2, int y2, int mode, Color &color ){
	if ( x2 < x )
		swap( x, x2 );
	if ( y2 > y )
		swap( y, y2 );
	Cor center = getCenter( x, y, x2, y2 );
	if (mode == FILL)
		filledEllipseRGBA( screen, center.x(), center.y(), center.x() - std::min( x, x2 ), center.y() - std::min( y, y2 ), color.red(), color.green(), color.blue(), color.alpha() );
	else if (mode == EMPTY)
		ellipseRGBA( screen, center.x(), center.y(), center.x() - std::min( x, x2 ), center.y() - std::min( y, y2), color.red(), color.green(), color.blue(), color.alpha() );
	else if ( abs( mode ) == EMPTY ){
		double a = abs( ( y2 - y ) / 2 );
		double b = abs( ( x2 - x ) / 2 );
		if ( !a || !b ){
			lineRGBA( screen, x, y, x2, y2, color.red(), color.green(), color.blue(), color.alpha() );
			return;
		}
		long long a2 = pow( a, 2 ), b2 = pow( b, 2 ), j2 = 0;
		for ( int i = -1 * b; i <= b; i++ ){
			long long i2 = pow( i, 2 );
			long long j = sqrt( a2 * ( b2 - i2 ) / b2 );
			lineRGBA( screen, i + x + b, j + y - a, i - 1 + x + b, j2 + y - a, color.red(), color.green(), color.blue(), color.alpha() );
			j2 = j;
		}
		j2 = 0;
		for ( int i = -1 * b; i <= b; i++ ){
			long long i2 = pow( i, 2 );
			long long j = -1 * sqrt( a2 * ( b2 - i2 ) / b2 );
			lineRGBA( screen, i + x + b, j + y - a, i - 1 + x + b, j2 + y - a, color.red(), color.green(), color.blue(), color.alpha() );
			j2 = j;
		}
	}
}
