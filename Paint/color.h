
#ifndef COLOR_H
#define COLOR_H

class Color{
	private:
		unsigned char _red, _green, _blue, _alpha;
	public:
		Color( unsigned char red = 0, unsigned char green = 0, unsigned char blue = 0, unsigned char alpha = 0 ) : _red( red ) , _green( green ) , _blue( blue ) , _alpha( alpha ) {}
/*		Color( Color &color ){
			this->setRed( color.red() );
			this->setGreen( color.green() );
			this->setBlue( color.blue() );
		}*/

		unsigned char red();
		unsigned char green();
		unsigned char blue();
		unsigned char alpha();

		void setRed( unsigned char );
		void setGreen( unsigned char );
		void setBlue( unsigned char );
		void setAlpha( unsigned char );
		
		void setColor( unsigned char, unsigned char, unsigned char, unsigned char = 0 );

		void operator=( Color color );
		
		bool operator!=( Color color );
		bool operator==( Color color );

};

#endif

