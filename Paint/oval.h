
#ifndef OVAL_H
#define OVAL_H

#include <cmath>

#include "tool.h"
#include "cor.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#define FILL 0
#define EMPTY 1

class Oval : public Tool{

	public:
		Oval( bool state, SDLKey key ) : Tool( state, key, "Oval" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};

#endif

