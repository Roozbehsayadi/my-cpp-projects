
#ifndef BUCKET_H
#define BUCKET_H

#include "tool.h"
#include "cor.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#include <cmath>
#include <queue>
#include <utility>

class Bucket : public Tool {
	private:
		bool **mark;
		Color selectedPointColor;
	public:
		Bucket( bool state, SDLKey key ) : Tool( state, key, "Bucket" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;

		friend void BFS( SDL_Surface*, int, int, Color );
		friend int calculate( SDL_Surface*, int, int );
		friend Color getColor( SDL_Surface*, int, int );
};

inline Color getColor( SDL_Surface *screen, int x, int y ){
	Uint32* pixel = (Uint32*) screen->pixels;
	Uint8* color = (Uint8*) &( pixel[ y * screen->w + x ] );
	return Color( (int) color[2], (int) color[1], (int) color[0] );
}

inline int calculate( SDL_Surface *screen, int x, int y ){
	return ( y * screen->w + x );
}

inline void check(SDL_Surface *screen , int x , int y , Color color , Color selectedPointColor , queue<pair<int , int> > &saf ){
	if ( x < 0 || x >= screen->w || y < 0 || y >= screen->h )	return;
	if ( getColor(screen , x , y) == color )	return;
	if ( getColor(screen , x , y) != selectedPointColor )	return;
	
	saf.push( { x, y } );
	pixelRGBA(screen , x , y , color.red() , color.green() , color.blue() , color.alpha() );

}

inline void BFS( SDL_Surface *screen, int x, int y, Color color, Color selectedPointColor ){
	queue< pair<int , int> >	saf;
	saf.push( { x, y } );
	
	pair<int , int>	front;
	while(!saf.empty()){
		front = saf.front();

		check(screen , front.first + 1 , front.second , color , selectedPointColor , saf);
		check(screen , front.first - 1 , front.second , color , selectedPointColor , saf);
		check(screen , front.first , front.second + 1 , color , selectedPointColor , saf);
		check(screen , front.first , front.second - 1 , color , selectedPointColor , saf);
/*		check(screen , front.first + 1 , front.second + 1 , color , selectedPointColor , saf);
		check(screen , front.first  - 1, front.second - 1 , color , selectedPointColor , saf);
		check(screen , front.first  - 1, front.second + 1 , color , selectedPointColor , saf);
		check(screen , front.first + 1, front.second - 1 , color , selectedPointColor , saf);*/

		saf.pop();
	}

}

#endif

