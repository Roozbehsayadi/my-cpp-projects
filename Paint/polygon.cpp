
#include "polygon.h"

void Polygon::add( const Cor &cor ){
	vx.push_back( cor.x() );
	vy.push_back( cor.y() );
	this->lastCor().setCor( cor );
	this->setAdded( true );
}

void Polygon::clear(){
	vx.clear();
	vy.clear();
	this->setAdded();
}

void Polygon::setAdded( bool added ){
	this->_added = added;
}

Cor &Polygon::lastCor(){
	return this->_lastCor;
}

bool Polygon::added(){
	return this->_added;
}

void Polygon::setLastCor( Cor cor ){
	this->lastCor().setCor( cor );
}

void Polygon::draw( SDL_Surface *screen, int count, int foo1, int foo2, int foo3, int mode, Color &color ){
	short int xs[ vx.size() ], ys[ vy.size() ];
	for (int i = 0; i < vx.size(); i++){
		xs[i] = vx[i];
		ys[i] = vy[i];
	}

/*	for (int i = 0; i < vx.size(); i++)
		cout << "(" << xs[i] << ", " << ys[i] << ')' << endl;*/

	if (mode == FILL)
		filledPolygonRGBA( screen, xs, ys, vx.size(), color.red(), color.green(), color.blue(), color.alpha() );
	else if (mode == EMPTY )
		polygonRGBA( screen, xs, ys, vx.size(), color.red(), color.green(), color.blue(), color.alpha() );
	vx.clear();
	vy.clear();
}
