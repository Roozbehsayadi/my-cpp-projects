
#include "eye.h"

void Eye::draw( SDL_Surface *screen, int x, int y, int, int, int, Color &color ){
	Uint32 *pixels = (Uint32*) screen->pixels;
	Uint8 *colors = (Uint8*) &( pixels[ y * screen->w + x ] );
	color.setColor( (int) colors[2], (int) colors[1], (int) colors[0] );
}
