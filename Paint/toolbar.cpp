
#include "toolbar.h"

Toolbar::Toolbar(){
	tool = new Tool*[TOOLSCOUNT];
}

Toolbar &Toolbar::set( int pos, Tool *tool ){
	this->tool[pos] = tool;
	return *this;
}

Tool *Toolbar::operator[]( int pos ){
	return tool[pos];
}

