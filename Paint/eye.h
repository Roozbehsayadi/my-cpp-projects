
#ifndef EYE_H
#define EYE_H

#include "tool.h"

#include "SDL/SDL.h"

class Eye : public Tool {
	public:
		Eye( bool state, SDLKey key ) : Tool( state, key, "EyeDropper" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};

#endif

