
#include "rectangle.h"

void Rectangle::draw( SDL_Surface *screen, int x, int y, int newX, int newY, int mode, Color &color ){
	if ( mode == FILL )
		boxRGBA( screen, x, y, newX, newY, color.red(), color.green(), color.blue(), color.alpha() );
	else if ( mode == EMPTY )
		rectangleRGBA( screen, x, y, newX, newY, color.red(), color.green(), color.blue(), color.alpha() );
}
