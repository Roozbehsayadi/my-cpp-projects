
#include "helper.h"

SDLKey Helper::key(){
	return this->_key;
}

void Helper::setKey( SDLKey key ){
	this->_key = key;
}

void Helper::showHelp( Toolbar &toolbar ){
	string s;
	s.append( "zenity --title \"UPaint's Keyboard Shortcuts\" --info --text \"" );
	for ( int i = 0; i < TOOLSCOUNT; i++)
		s.append( toolbar[i]->name() ).append( " -> " ).append( intToStr( toolbar[i]->key() + ('A' - 'a' ) ) ).append( "\n" );
	s.append( "\"" );
	system( s.c_str() );
}

