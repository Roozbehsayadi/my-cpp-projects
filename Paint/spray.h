
#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#include <cmath>

class Spray : public Tool {
	public:
		Spray( bool state, SDLKey key ) : Tool( state, key, "Spray" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};
