
#include "color.h"

unsigned char Color::red(){
	return this->_red;
}

unsigned char Color::green(){
	return this->_green;
}

unsigned char Color::blue(){
	return this->_blue;
}

unsigned char Color::alpha(){
	return this->_alpha;
}

void Color::setRed( unsigned char red ){
	this->_red = red;
}

void Color::setGreen( unsigned char green ){
	this->_green = green;
}

void Color::setBlue( unsigned char blue ){
	this->_blue = blue;
}

void Color::setAlpha( unsigned char alpha ){
	this->_alpha = alpha;
}

void Color::setColor( unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha ){
	this->setRed( red );
	this->setGreen( green );
	this->setBlue( blue );
	this->setAlpha( alpha );
}

void Color::operator=( Color color ){
	this->setRed( color.red() );
	this->setGreen( color.green() );
	this->setBlue( color.blue() );
	this->setAlpha( color.alpha() );
}

bool Color::operator!=( Color color ){
	if ( this->red() != color.red() || this->green() != color.green() || this->blue() != color.blue() || this->alpha() != color.alpha() )
		return true;
	return false;
}

bool Color::operator==( Color color ){
	if ( this->red() == color.red() && this->green() == color.green() && this->blue() == color.blue() && this->alpha() == color.alpha() )
		return true;
	return false;
}

