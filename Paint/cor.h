
#ifndef COR_H
#define COR_H

class Cor{
	private:
		double _x, _y;
	public:

		Cor( double = 0, double = 0 );

		double x() const;
		double y() const;

		void setX( double = 0 );
		void setY( double = 0 );

		void setCor( double = 0, double = 0 );
		void setCor( const Cor& );
};

inline Cor getCenter( int x1, int y1, int x2, int y2 ){
	return Cor( (x1 + x2) / 2, (y1 + y2) / 2 );
}

#endif

