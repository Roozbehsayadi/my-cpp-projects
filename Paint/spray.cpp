
#include "spray.h"

void Spray::draw( SDL_Surface *screen, int x, int y, int, int, int radius, Color &color ){
	for (int i = x - (radius / 2); i <= x + (radius / 2); i++){
		for (int j = y - (radius / 2); j <= y + (radius / 2); j++){
			int distance = sqrt( pow( x - i, 2 ) + pow( y - j, 2 ) );
			if (distance <= radius / 2){
				int foo = rand() % 75;
				if (foo == 0)
					pixelRGBA( screen, i, j, color.red(), color.green(), color.blue(), color.alpha() );
			}
		}
	}
}

