
#ifndef MENU_H
#define MENU_H

#include "toolbar.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>

#define OUTPUTFILE /tmp/Menu

class Menu{
	private:
		SDLKey _key;
	public:
		Menu( SDLKey key ) : _key( key ) {}

		void setupMenu( Toolbar &toolbar );

		void setKey( SDLKey = SDLK_UNKNOWN );

		SDLKey key();
};

#endif

