
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#define FILL 0
#define EMPTY 1

class Triangle : public Tool {
	public:
		Triangle( bool state, SDLKey key ) : Tool( state, key, "Triangle" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};

#endif

