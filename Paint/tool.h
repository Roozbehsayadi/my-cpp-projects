
#ifndef TOOL_H
#define TOOL_H

#include "SDL/SDL.h"

#include "color.h"
#include "cor.h"

#include <iostream>
#include <string>

using namespace std;

class Tool{
	protected:
		int _radius;
		bool _isSelected;
		string _address, _name;
		SDLKey _key;
	public:
		Tool( bool isSelected = false, SDLKey key = SDLK_UNKNOWN, string name = "" ) : _radius( 0 ) , _isSelected( isSelected ) , _key( key), _name( name ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color );

//		Polygon's Functions
		virtual void add( const Cor& );
		virtual void clear();
		virtual Cor &lastCor();
		virtual bool added();
		virtual void setLastCor( Cor );
//		End of Polygon's Functions

		int radius();
		bool isSelected();
		string address();
		string name();
		SDLKey key();

		void setRadius( const int& = 0 );
		void setSelected( const bool& = false );
		void setAddress( const string& );
		void setName( const string& );
		void setKey( SDLKey );
};

#endif

