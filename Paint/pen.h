
#ifndef PEN_H
#define PEN_H

#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#include <cmath>

class Pen : public Tool{
	public:
		
		Pen( bool statues, SDLKey key ) : Tool( statues, key, "Pen" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};

#endif

