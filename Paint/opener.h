
#ifndef OPENER_H
#define OPENER_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

#define OUTPUTFILE /tmp/Opener

class Opener {
	private:
		SDLKey _key;
	public:
		Opener( SDLKey key ) : _key( key ) {}

		void setKey( SDLKey );

		SDLKey key();

		void browse( SDL_Surface* );
};

#endif

