
#include "line.h"

void Line::draw( SDL_Surface *screen, int x1, int y1, int x2, int y2, int radius, Color &color ){
//	lineRGBA( screen, x1, y1, x2, y2, color.red(), color.green(), color.blue(), 255 );

	if (x1 == x2 && y1 == y2){
		filledEllipseRGBA( screen, x1, y1, radius / 2, radius / 2, color.red(), color.green(), color.blue(), color.alpha() );
		return;
	}
	
	int hyp = sqrt( pow( x2 - x1, 2 ) + pow( y2 - y1, 2) );
	double alpha;
	double sinos = asin( (y1 - y2) / hyp );
	double cosin = acos( (x1 - x2) / hyp );
/*	if (sinos >= 0 && cosin >= 0)
		alpha = asin( sinos );
	if (sinos >= 0 && cosin <= 0)
		alpha = asin( sinos ) + (M_PI / 2 );
	if (sinos <= 0 && cosin <= 0)
		alpha = acos( cosin );
	if (sinos <= 0 && cosin >= 0)
		alpha = acos( cosin ) + (M_PI / 2 );*/

//	cout << "Alpha:" << alpha * 180 / M_PI << endl;

	short int vx[4], vy[4];

	vx[0] = x1 - ( sin( alpha ) * radius / 2 );
	vy[0] = y1 - ( cos( alpha ) * radius / 2 );

	vx[1] = x1 + ( sin( alpha ) * radius / 2 );
	vy[1] = y1 + ( cos( alpha ) * radius / 2 );

	vx[2] = x2 + ( sin( alpha ) * radius / 2 );
	vy[2] = y2 + ( cos( alpha ) * radius / 2 );

	vx[3] = x2 - ( sin( alpha ) * radius / 2 );
	vy[3] = y2 - ( cos( alpha ) * radius / 2 );

	filledPolygonRGBA( screen, vx, vy, 4, color.red(), color.green(), color.blue(), color.alpha() );

}
