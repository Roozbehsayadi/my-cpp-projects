
#include "cor.h"

Cor::Cor( double x, double y ){
	this->setCor( x, y );
}

double Cor::x() const{
	return this->_x;
}

double Cor::y() const{
	return this->_y;
}

void Cor::setX( double x ){
	this->_x = x;
}

void Cor::setY( double y ){
	this->_y = y;
}

void Cor::setCor( double x, double y ){
	this->setX( x );
	this->setY( y );
}

void Cor::setCor( const Cor &cor ){
	this->setX( cor.x() );
	this->setY( cor.y() );
}

