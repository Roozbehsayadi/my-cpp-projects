
#include "bucket.h"

void Bucket::draw( SDL_Surface *screen, int x, int y, int, int, int, Color &color ){
	selectedPointColor = getColor( screen, x, y );
	if ( selectedPointColor == color )
		return;
	BFS( screen, x, y, color, this->selectedPointColor );
}
