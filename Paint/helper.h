
#ifndef HELPER_H
#define HELPER_H

#include "toolbar.h"

#include <string>

class Helper {
	
	friend string intToStr( int );

	private:
		SDLKey _key;
	public:
		Helper( SDLKey key ) : _key ( key ) {}

		void showHelp( Toolbar& );

		SDLKey key();

		void setKey( SDLKey = SDLK_UNKNOWN );
};

inline string intToStr( int input ){
	string output;
	char c = char ( input );
	output = c;
	return output;
}

#endif

