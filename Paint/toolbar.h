
#ifndef TOOLBAR_H
#define TOOLBAR_H

#include "SDL/SDL.h"

#include "pen.h"
#include "rectangle.h"
#include "oval.h"
#include "polygon.h"
#include "triangle.h"
#include "line.h"
#include "spray.h"
#include "filler.h"
#include "bucket.h"
#include "eye.h"

/*#define TOOLSCOUNT 10
#define PEN 0
#define RECTANGLE 1
#define OVAL 2
#define POLYGON 3
#define TRIANGLE 4
#define LINE 5
#define SPRAY 6
#define FILLER 7
#define BUCKET 8
#define EYE 9*/

#define ICONSFOLDER ("icons")

enum Tools{
	PEN,
	RECTANGLE,
	OVAL,
	POLYGON,
	TRIANGLE,
	LINE,
	SPRAY,
	FILLER,
	BUCKET, 
	EYE,
	TOOLSCOUNT
};

class Toolbar{
	private:
		Tool **tool;
	public:
		Toolbar();

		Toolbar &set( int, Tool* );
		Tool *operator[]( int );

};

#endif

