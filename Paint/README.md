# Paint

## Installing Requirements

You need SDL1.2 and zenity to run this program.

```shell
sudo apt install libsdl1.2-dev libsdl-gfx1.2-dev libsdl-image1.2-dev zenity
```

## Using the Program

At the beginning you'll see a white window. This is your canvas!

![First Page](https://www.dl.dropboxusercontent.com/s/ve1n52tgyqggtes/First%20Page.png?dl=0)

Here, you can do one of the followings:

* **m**: By pressing this key, You'll see a list of available tools (Such as pen, rectangle, etc). The eyedropper tool doesn't work though.

* **h**: By pressing this key, A help menu will pop up to show you keyboard shortcuts for the tools.

* **ctrl+z**: Yes! It has undo!

* **c**: Brings up the color *menu*. Click on the color you want, and use *Esc* to return to the canvas.

* **+ and -**: Change the brush radius.

* **ctrl+s**: Saves your art.

* **ctrl+o**: Open an existing art.

### Tips and Tricks

* When you're drawing shapes (such as triangle or rectangle), use left mouse button to draw a filled shape, and right mouse button to draw the shape with no fill.

* When using Polygon, keep adding vertices. Press *space* when you were finished to connect first and last vertices. press *Esc* to leave the polygon in the current state.

## More Screenshots

![Screenshot](https://www.dl.dropboxusercontent.com/s/roe78hlbxyynvsq/Paint.png?dl=0)

