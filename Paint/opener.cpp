
#include "opener.h"

void Opener::setKey( SDLKey key ){
	this->_key = key;
}

SDLKey Opener::key(){
	return this->_key;
}

void Opener::browse( SDL_Surface *screen ){
	SDL_Surface *image;
	system( "zenity --file-selection >OUTPUTFILE --file-filter=\'Picture Files | *.jpg *png *.bmp\'" );
	string s;
	ifstream fin( "OUTPUTFILE" );
	fin >> s;
	image = IMG_Load( s.c_str() );
	Uint32 flags = screen->flags;
	SDL_FreeSurface( screen );
	screen = SDL_SetVideoMode( image->w, image->h, 32, flags );
	SDL_BlitSurface( image, NULL, screen, NULL );
	SDL_Flip( screen );
}

