
#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#define FILL 0
#define EMPTY 1

class Rectangle : public Tool{
	public:
		Rectangle( bool state, SDLKey key ) : Tool( state, key, "Rectangle" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};

#endif

