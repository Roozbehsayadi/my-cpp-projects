
#ifndef POLYGON_H
#define POLYGON_H

#include "tool.h"
#include "cor.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#include <vector>

#define FILL 0
#define EMPTY 1

class Polygon : public Tool {
	private:
		vector <short int> vx, vy;
		Cor _lastCor;
		bool _added;
	public:
		
		Polygon( bool state, SDLKey key, Cor cor ) : Tool( state, key, "Polygon" ), _added( false ) {
			this->setLastCor( cor );
		}

		void add( const Cor& );

		void setAdded( bool = false );

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
		virtual void clear() override;
		virtual Cor &lastCor() override;
		virtual bool added() override;
		virtual void setLastCor( Cor ) override;
};

#endif

