
#include "triangle.h"

void Triangle::draw( SDL_Surface *screen, int x1, int y1, int x2, int y2, int mode, Color &color ){
	if (mode == FILL)
		filledTrigonRGBA( screen, x1, y1, x2, y2, x1, y2, color.red(), color.green(), color.blue(), color.alpha() );
	else if (mode == EMPTY)
		trigonRGBA( screen, x1, y1, x2, y2, x1, y2, color.red(), color.green(), color.blue(), color.alpha() );
}
