
#ifndef SAVER_H
#define SAVER_H

#include "SDL/SDL.h"

#include <iostream>
#include <fstream>
#include <string>

#define OUTPUTFILE tmp/saveLocation

class Saver {
	private:
		SDLKey _key;
	public:
		Saver( SDLKey key ) : _key( key ) {}
		
		void saveImage( SDL_Surface* );

		void setKey( SDLKey = SDLK_UNKNOWN );

		SDLKey key();
};

#endif
