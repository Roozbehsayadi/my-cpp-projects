
#include "tool.h"

void Tool::draw( SDL_Surface*, int, int, int, int, int, Color& ){
	std::cout << "COUDLN'T IDENTIFY TYPE OF SHAPE!\n";
}

void Tool::add( const Cor& ){
	cout << "I AM NOT A POLYGON!" << endl;
}

void Tool::clear(){
	cout << "I AM NOT A POLYGON!" << endl;
}

Cor &Tool::lastCor(){
	cout << "I AM NOT A POLYGON!" << endl;
}

bool Tool::added(){
	cout << "I AM NOT A POLYGON!" << endl;
}

void Tool::setLastCor( Cor ){
	cout << "I AM NOT A POLYGON!" << endl;
}

int Tool::radius(){
	return this->_radius;
}

bool Tool::isSelected(){
	return this->_isSelected;
}

string Tool::address(){
	return this->_address;
}

string Tool::name(){
	return this->_name;
}

SDLKey Tool::key(){
	return this->_key;
}

void Tool::setRadius( const int &radius ){
	this->_radius = radius;
}

void Tool::setSelected( const bool &isSelected ){
	this->_isSelected = isSelected;
}

void Tool::setAddress( const string &address ){
	this->_address = address;
}

void Tool::setName( const string &name ){
	this->_name = name;
}

void Tool::setKey( SDLKey key ){
	this->_key = key;
}

