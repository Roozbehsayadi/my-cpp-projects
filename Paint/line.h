
#ifndef LINE_H
#define LINE_H

#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

#include <cmath>

using namespace std;

class Line : public Tool {
	public:
		Line( bool state, SDLKey key ) : Tool( state, key, "Line" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;

};

#endif

