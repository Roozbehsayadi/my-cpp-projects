
#include "menu.h"

void Menu::setupMenu( Toolbar &toolbar ){
	string s;
	s = "zenity --title=Menu --width=640 --height=480 --list --imagelist --text=ChooseATool --column=Select --column=Tool ";
	for ( int i = 0; i < TOOLSCOUNT; i++)
		s.append( ICONSFOLDER ).append( "/" ).append( toolbar[i]->name() ).append( ".jpg " ).append( toolbar[i]->name() ).append( " " );
	s.append( ">OUTPUTFILE" );
	system( s.c_str() );
	cout << s << endl;
	ifstream fin( "OUTPUTFILE" );
	fin >> s;
	for ( int i = 0; i < TOOLSCOUNT; i++ ){
		string target;
		target.append( ICONSFOLDER ).append( "/" ).append( toolbar[i]->name() ).append( ".jpg" );
		if ( s == target ){
			for ( int j = 0; j < TOOLSCOUNT; j++ )
				toolbar[j]->setSelected();
			toolbar[i]->setSelected( true );
		}
	}
	system( "rm OUTPUTFILE" );
}

void Menu::setKey( SDLKey key ){
	this->_key = key;
}

SDLKey Menu::key(){
	return this->_key;
}

