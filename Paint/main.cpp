
#include "toolbar.h"
#include "menu.h"
#include "saver.h"
#include "helper.h"
#include "opener.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"
#include "SDL/SDL_image.h"

#define TEMPADDRESS ("/tmp/paint/screen")
#define BACKUPFOLDER ("/tmp/paint/paint-backup")
#define POLYGONADDRESS ("/tmp/paint/polygon")

#include <vector>
#include <cmath>
#include <string>
#include <fstream>

using namespace std;

void getBackup( SDL_Surface *screen, const char *address, int *counter = NULL );

void drawColorCircle( SDL_Surface *screen ){
	boxRGBA( screen, 0, 0, screen->w, screen->h, 0, 0, 0, 255 );
	for (int i = 0; i < 10; i++){
		int x1, x2, y1, y2, centreX = screen->w/2, centreY = screen->h/2, r, n = 255*6;
		float theta = 0;
		r = 10;
		x1 = ceil(r * cos(2*M_PI*0/n + theta) + centreX);
		y1 = ceil(r * sin(2*M_PI*0/n + theta) + centreY);
		r += 55;
		x2 = ceil(r * cos(2*M_PI*0/n + theta) + centreX);
		y2 = ceil(r * sin(2*M_PI*0/n + theta) + centreY);
		lineRGBA(screen, x1, y1, x2, y2, 255, 0, 0, 255);
		int red = 0, green = 0, blue = 0;
		for (int i = 0; i < 255 * 6; i++){
			r = 10;
			x1 = ceil(r * cos(2*M_PI*i/n + theta) + centreX);
			y1 = ceil(r * sin(2*M_PI*i/n + theta) + centreY);
			r += 55;
			x2 = ceil(r * cos(2*M_PI*i/n + theta) + centreX);
			y2 = ceil(r * sin(2*M_PI*i/n + theta) + centreY);
			if (i <= 255){
				lineRGBA(screen, x1, y1, x2, y2, 255, 0, blue, 255);
				blue++;
			}
			else if (i <= 255*2){
				lineRGBA(screen, x1, y1, x2, y2, red, 0, 255, 255);
				red--;
			}
			else if (i <= 255*3){
				lineRGBA(screen, x1, y1, x2, y2, 0, green, 255, 255);
				green++;
			}
			else if (i <= 255*4){
				lineRGBA(screen, x1, y1, x2, y2, 0, 255, blue, 255);
				blue--;
			}
			else if (i <= 255*5){
				lineRGBA(screen, x1, y1, x2, y2, red, 255, 0, 255);
				red++;
			}
			else if (i <= 255*6){
				lineRGBA(screen, x1, y1, x2, y2, 255, green, 0, 255);
				green--;
				}
			}
			for (int i = 0; i < 255 * 6; i++){
			r = 40;
			x1 = floor(r * cos(2*M_PI*i/n + theta) + centreX);
			y1 = floor(r * sin(2*M_PI*i/n + theta) + centreY);
			r += 55;
			x2 = floor(r * cos(2*M_PI*i/n + theta) + centreX);
			y2 = floor(r * sin(2*M_PI*i/n + theta) + centreY);
			if (i <= 255){
				lineRGBA(screen, x1, y1, x2, y2, 255, 0, blue, 255);
				blue++;
			}
			else if (i <= 255*2){
				lineRGBA(screen, x1, y1, x2, y2, red, 0, 255, 255);
				red--;
			}
			else if (i <= 255*3){
				lineRGBA(screen, x1, y1, x2, y2, 0, green, 255, 255);
				green++;
			}
			else if (i <= 255*4){
				lineRGBA(screen, x1, y1, x2, y2, 0, 255, blue, 255);
				blue--;
			}
			else if (i <= 255*5){
				lineRGBA(screen, x1, y1, x2, y2, red, 255, 0, 255);
				red++;
			}
			else if (i <= 255*6){
				lineRGBA(screen, x1, y1, x2, y2, 255, green, 0, 255);
				green--;
			}
		}
	}
	SDL_Flip( screen );
}

void closeProgram( SDL_Surface *screen, Toolbar &toolbar ){
	for (int i = 0; i < TOOLSCOUNT; i++)
		delete toolbar[ i ];
	SDL_FreeSurface( screen );
	SDL_Quit();
	char c[2000];
	system( "rm -r /tmp/paint" );
	exit( 0 );
}

void checkRadius( SDL_Event event, int &radius ){
	
	if ( event.type == SDL_KEYDOWN && ( event.key.keysym.sym == SDLK_KP_PLUS || event.key.keysym.sym == SDLK_PLUS ) )
		radius++;
	
	if ( event.type == SDL_KEYDOWN && ( event.key.keysym.sym == SDLK_KP_MINUS || event.key.keysym.sym == SDLK_MINUS ) )
		radius--;
	
	if ( radius <= 0 )
		radius++;
}

void checkMouseButtonHold( SDL_Event event, bool &isLeftButtonHold, bool &isRightButtonHold, int &x, int &y ){
	
	if ( event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT ){
		x = event.button.x;
		y = event.button.y;
		isLeftButtonHold = true;
	}
	
	else if (event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_LEFT )
		isLeftButtonHold = false;
	
	
	if ( event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_RIGHT ){
		x = event.button.x;
		y = event.button.y;
		isRightButtonHold = true;
	}
	else if ( event.type == SDL_MOUSEBUTTONUP && event.button.button == SDL_BUTTON_RIGHT )
		isRightButtonHold = false;
}

void setCordinate( int &x, int &y, Uint16 &newX, Uint16 &newY ){
	x = newX;
	y = newY;
}

void checkQuit( SDL_Event event, SDL_Surface *screen, Toolbar &toolbar ){
	if (event.type == SDL_QUIT)
		closeProgram( screen, toolbar );
	if ( event.type == SDL_KEYDOWN )
		if ( event.key.keysym.sym == SDLK_F4 )
			if ( event.key.keysym.mod != 0 && ( event.key.keysym.mod & KMOD_ALT ) )
				closeProgram( screen, toolbar );
}

void loadFromBackup( SDL_Surface *screen, const char *address, int *counter = NULL ){
	char c[200];
	if (counter != NULL)
		sprintf( c, "%s%d", address, *counter );
	else sprintf( c, "%s", address );
	SDL_Surface *image = IMG_Load( c );
	SDL_BlitSurface( image, NULL, screen, NULL );
	SDL_Flip( screen );
}

int checkToolSelection( SDL_Event event, Toolbar &toolbar ){
	if ( event.type == SDL_KEYDOWN ){
		for (int i = 0; i < TOOLSCOUNT; i++){
			if ( event.key.keysym.sym == toolbar[i]->key() && ( event.key.keysym.mod == KMOD_NONE || event.key.keysym.mod == KMOD_NUM || event.key.keysym.mod == KMOD_CAPS) ){
				cout << i << endl;
				for (int j = 0; j < TOOLSCOUNT; j++)
					toolbar[j]->setSelected();
				toolbar[i]->setSelected( true );
				return i;
			}
		}
	}
}

void checkPen( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, int radius, bool leftButtonHold, Color color ){
	if ( event.type == SDL_MOUSEMOTION && leftButtonHold ){
		toolbar[PEN]->draw( screen, x, y, event.motion.x, event.motion.y, radius, color );
		setCordinate( x, y, event.motion.x, event.motion.y );
		SDL_Flip( screen );
	}
}

void checkRectangle( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, Color color, bool leftButtonHold, bool rightButtonHold, int &counter ){
	if ( event.type == SDL_MOUSEMOTION ){
		if ( rightButtonHold ){
			SDL_ShowCursor( SDL_DISABLE );
			char c[200];
			sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
			loadFromBackup( screen, c, &counter );
			toolbar[RECTANGLE]->draw( screen, x, y, event.motion.x, event.motion.y, EMPTY, color );
			SDL_Flip( screen );
		}
		if ( leftButtonHold ){
			SDL_ShowCursor( SDL_DISABLE );
			char c[200];
			sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
			loadFromBackup( screen, c, &counter );
			toolbar[RECTANGLE]->draw( screen, x, y, event.motion.x, event.motion.y, FILL, color );
			SDL_Flip( screen );
		}
	}
	if ( event.type == SDL_MOUSEBUTTONUP ){
		SDL_ShowCursor( SDL_ENABLE );
		if (event.button.button == SDL_BUTTON_LEFT )
			toolbar[RECTANGLE]->draw( screen, x, y, event.motion.x, event.motion.y, FILL, color );
		else if (event.button.button == SDL_BUTTON_RIGHT )
			toolbar[RECTANGLE]->draw( screen, x, y, event.motion.x, event.motion.y, EMPTY, color );
			setCordinate( x, y, event.button.x, event.button.y );
		SDL_Flip( screen );
	}
}

void checkOval( SDL_Event event, SDL_Surface *screen, int &x, int &y, bool leftButtonHold, bool rightButtonHold, bool &shift, Toolbar toolbar, Color color, int &counter ){
	if ( event.type == SDL_MOUSEMOTION ){
		if ( leftButtonHold ){
			char c[200];
			sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
			loadFromBackup( screen, c, &counter );
			if ( !shift )
				toolbar[OVAL]->draw( screen, x, y, event.motion.x, event.motion.y, FILL, color );
/*			else 
				toolbar[OVAL]->draw( screen, x, y, */
			SDL_Flip( screen );
		}
		if ( rightButtonHold ){
			char c[200];
			sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
			loadFromBackup( screen, c, &counter );
			toolbar[OVAL]->draw( screen, x, y, event.motion.x, event.motion.y, EMPTY, color );
			SDL_Flip( screen );
		}
	}
	if ( event.type == SDL_MOUSEBUTTONUP ){
		char c[200];
		sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
		loadFromBackup( screen, c, &counter );
		if (event.button.button == SDL_BUTTON_LEFT )
			toolbar[OVAL]->draw( screen, x, y, event.button.x, event.button.y, FILL, color );
		else if (event.button.button == SDL_BUTTON_RIGHT )
			toolbar[OVAL]->draw( screen, x, y, event.button.x, event.button.y, EMPTY * -1, color );
			setCordinate( x, y, event.button.x, event.button.y );
		SDL_Flip( screen );
	}
}

bool checkPolygon( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, Color color, int &counter ){
	loadFromBackup( screen, BACKUPFOLDER, &counter );
	static int mode;
	if ( event.type == SDL_MOUSEMOTION ){
		if ( toolbar[POLYGON]->added() ){
			SDL_ShowCursor( SDL_DISABLE );
			loadFromBackup( screen, POLYGONADDRESS );
			lineRGBA( screen, toolbar[POLYGON]->lastCor().x(), toolbar[POLYGON]->lastCor().y(), event.motion.x, event.motion.y, color.red(), color.green(), color.blue(), 150 );
			SDL_Flip( screen );
		}
	}
	if ( event.type == SDL_MOUSEBUTTONDOWN ){
		if ( event.button.button == SDL_BUTTON_LEFT )
			mode = 0;
		else if ( event.button.button == SDL_BUTTON_RIGHT )
			mode = 1;
		toolbar[POLYGON]->add( Cor( event.button.x, event.button.y ) );
		toolbar[POLYGON]->setLastCor( Cor( event.button.x, event.button.y ) );
		return true;
	}
	if ( event.type == SDL_KEYDOWN ){
		if ( event.key.keysym.sym == SDLK_SPACE ){
			SDL_ShowCursor( SDL_ENABLE );
			loadFromBackup( screen, POLYGONADDRESS );
			toolbar[POLYGON]->draw( screen, 0, 0, 0, 0, mode, color );
			SDL_Flip( screen );
			toolbar[POLYGON]->clear();
			return true;
		}
		else {
			SDL_ShowCursor( SDL_ENABLE );
			toolbar[POLYGON]->clear();
		}
	}
	return false;
}

void checkTriangle( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, bool leftButtonHold, bool rightButtonHold, Color color, int &counter ){
	if ( event.type == SDL_MOUSEMOTION ){
		if ( leftButtonHold ){
			SDL_ShowCursor( SDL_DISABLE );
			char c[200];
			sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
			loadFromBackup( screen, c, &counter );
			toolbar[TRIANGLE]->draw( screen, x, y, event.motion.x, event.motion.y, FILL, color );
			SDL_Flip( screen );
		}
		if ( rightButtonHold ){
			SDL_ShowCursor( SDL_DISABLE );
			char c[200];
			sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
			loadFromBackup( screen, c, &counter );
			toolbar[TRIANGLE]->draw( screen, x, y, event.motion.x, event.motion.y, EMPTY, color );
			SDL_Flip( screen );
		}
	}
	if (event.type == SDL_MOUSEBUTTONUP){
		SDL_ShowCursor( SDL_ENABLE );
		if (event.button.button == SDL_BUTTON_LEFT)
			toolbar[TRIANGLE]->draw( screen, x, y, event.button.x, event.button.y, FILL, color );
		else if (event.button.button == SDL_BUTTON_RIGHT)
			toolbar[TRIANGLE]->draw( screen, x, y, event.button.x, event.button.y, EMPTY, color );

		setCordinate( x, y, event.button.x, event.button.y );
		SDL_Flip( screen );
	}
}

void checkLine( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, int radius, Color color, bool leftButtonHold, bool rightButtonHold, int &counter ){

	if ( event.type == SDL_MOUSEMOTION && leftButtonHold ){
		SDL_ShowCursor( SDL_DISABLE );
		char c[200];
		sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
		loadFromBackup( screen, c, &counter );
		toolbar[LINE]->draw( screen, x, y, event.motion.x, event.motion.y, radius, color );
		SDL_Flip( screen );
	}
	if ( event.type == SDL_MOUSEMOTION && rightButtonHold ){
		SDL_ShowCursor( SDL_DISABLE );
		char c[200];
		sprintf( c, "%s%s", BACKUPFOLDER, "/screen" );
		loadFromBackup( screen, c, &counter );
		if ( abs( event.motion.x - x ) > abs( event.motion.y - y ) )
			toolbar[LINE]->draw( screen, x, y, event.motion.x, y, radius, color );
		else
			toolbar[LINE]->draw( screen, x, y, x, event.motion.y, radius, color );
		SDL_Flip( screen );
	}
	
	if ( event.type == SDL_MOUSEBUTTONUP ){
		SDL_ShowCursor( SDL_ENABLE );
		if (event.button.button == SDL_BUTTON_LEFT)
			toolbar[LINE]->draw( screen, x, y, event.button.x, event.button.y, radius, color );
		else if (event.button.button == SDL_BUTTON_RIGHT){
			if ( abs( event.button.x - x ) > abs( event.button.y - y ) )
				toolbar[LINE]->draw( screen, x, y, event.button.x, y, radius, color );
			else 
				toolbar[LINE]->draw( screen, x, y, x, event.button.y, radius, color );
		}
		setCordinate( x, y, event.button.x, event.button.y );
		SDL_Flip( screen );
	}
}

void getBackup( SDL_Surface *screen, const char *address, int *counter ){
	char c[200];
	if (counter != NULL){
		*counter += 1;
		sprintf( c, "%s%s%d", address, "/screen", *counter );
	}
	else sprintf( c, "%s", address );
	cout << c << endl;
	SDL_SaveBMP( screen, c );
}

void checkSpray( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, int radius, Color color, bool leftButtonHold ){
	if ( event.type == SDL_MOUSEMOTION ){
		if (leftButtonHold){
			toolbar[SPRAY]->draw( screen, event.motion.x, event.motion.y, 0, 0, radius, color );
			setCordinate( x, y, event.motion.x, event.motion.y );
		}
	}
	else if ( event.type == SDL_MOUSEBUTTONDOWN ){
		toolbar[SPRAY]->draw( screen, event.button.x, event.button.y, 0, 0, radius, color );
		setCordinate( x, y, event.button.x, event.button.y );
	}
	SDL_Flip( screen );
}

void checkFiller( SDL_Event event, SDL_Surface *screen, Toolbar &toolbar, Color color ){
	toolbar[FILLER]->draw( screen, 0, 0, 0, 0, 0, color );
	toolbar[FILLER]->setSelected();
	SDL_Flip( screen );
}

void checkBucket( SDL_Event event, SDL_Surface *screen, Toolbar &toolbar, Color color ){
	if ( event.type == SDL_MOUSEBUTTONDOWN ){
		int x = event.button.x, y = event.button.y;
		toolbar[BUCKET]->draw( screen, x, y, 0, 0, 0, color );
		SDL_Flip( screen );
	}
}

void checkEye( SDL_Event event, SDL_Surface *screen, Toolbar &toolbar, Color &color ){
	if ( event.type == SDL_MOUSEBUTTONDOWN ){
		toolbar[EYE]->draw( screen, event.button.x, event.button.y, 0, 0, 0, color );
	}
}

void checkDrawing( SDL_Event event, SDL_Surface *screen, int &x, int &y, Toolbar toolbar, bool leftButtonHold, bool rightButtonHold, bool &shift, int radius, Color &color, int &counter ){

	bool done = false;
	if ( ( event.type == SDL_MOUSEBUTTONUP && !toolbar[POLYGON]->isSelected() ) || ( event.type == SDL_KEYDOWN && toolbar[FILLER]->isSelected() ) )
		done = true;

	if ( toolbar[PEN]->isSelected() )	checkPen( event, screen, x, y, toolbar, radius, leftButtonHold, color );
	if ( toolbar[RECTANGLE]->isSelected() )	checkRectangle( event, screen, x, y, toolbar, color, leftButtonHold, rightButtonHold, counter );
	if ( toolbar[OVAL]->isSelected() )	checkOval( event, screen, x, y, leftButtonHold, rightButtonHold, shift, toolbar, color, counter );
	if ( toolbar[POLYGON]->isSelected() )
		if ( checkPolygon( event, screen, x, y, toolbar, color, counter ) )
			done = true;
	if ( toolbar[TRIANGLE]->isSelected() )	checkTriangle( event, screen, x, y, toolbar, leftButtonHold, rightButtonHold, color, counter );
	if ( toolbar[LINE]->isSelected() )	checkLine( event, screen, x, y, toolbar, radius, color, leftButtonHold, rightButtonHold, counter );
	if ( toolbar[SPRAY]->isSelected() ) checkSpray( event, screen, x, y, toolbar, radius, color, leftButtonHold );
	if ( toolbar[FILLER]->isSelected() )	checkFiller( event, screen, toolbar, color );
	if ( toolbar[BUCKET]->isSelected() )	checkBucket( event, screen, toolbar, color );
	if ( toolbar[EYE]->isSelected() )	checkEye( event, screen, toolbar, color );

	if ( done ){
		if (toolbar[POLYGON]->isSelected() )	getBackup( screen, POLYGONADDRESS );
		if ( !toolbar[POLYGON]->added() )	getBackup( screen, BACKUPFOLDER, &counter );
		system( "cd /tmp/paint/paint-backup && ls -F |grep -v / | wc -l >/tmp/paint/paint-backup/filesCount" );
		ifstream fin( "/tmp/paint/paint-backup/filesCount" );
		int filesCount;
		fin >> filesCount;
		for (int i = counter + 1; i < filesCount - 1; i++){
			char c[200];
			sprintf( c, "rm /tmp/paint-backup/screen%d", i );
			system( c );
		}
	}
}

void setUpToolbar( Toolbar &toolbar, Tool *pen, Tool *rectangle, Tool *oval, Tool *polygon, Tool *triangle, Tool *line, Tool *spray, Tool *filler, Tool *bucket, Tool *eye ){
	toolbar.set( PEN, pen ).set( RECTANGLE, rectangle ).set( OVAL, oval ).set( POLYGON, polygon ).set( TRIANGLE, triangle ).set( LINE, line ).set( SPRAY, spray ).set( FILLER, filler ).set( BUCKET, bucket ).set( EYE, eye );
}

void checkColor( SDL_Event event, SDL_Surface *screen, Color &color, bool leftButtonHold, Toolbar &toolbar ){
	if ( event.type == SDL_KEYDOWN ){
		if ( event.key.keysym.sym == SDLK_c && ( event.key.keysym.mod != 0 && ( event.key.keysym.mod & KMOD_SHIFT ) ) ){
			getBackup( screen, TEMPADDRESS );
			const int x = screen->w, y = screen->h;
			Uint32 flags = screen->flags;
			SDL_Surface *colorSurface = SDL_SetVideoMode( 500, 4 * 100, 32, SDL_HWSURFACE | SDL_NOFRAME );
			boxRGBA( colorSurface, 0, 0, 255, 100, 255, 0, 0, 255 );
			boxRGBA( colorSurface, 0, 100, 255, 200, 0, 255, 0, 255 );
			boxRGBA( colorSurface, 0, 200, 255, 300, 0, 0, 255, 255 );
			boxRGBA( colorSurface, 0, 300, 255, 400, 255, 255, 255, 255 );
			SDL_Flip( colorSurface );
			SDL_Event event;
			boxRGBA( colorSurface, 300, 0, 500, 300, color.red(), color.green(), color.blue(), 255 );
			boxRGBA( screen, (int) color.red() - 1, 0, (int) color.red() + 1, 100, 0, 0, 0, 255 );
			boxRGBA( screen, (int) color.green() - 1, 100, (int) color.green() + 1, 200, 0, 0, 0, 255 );
			boxRGBA( screen, (int) color.blue() - 1, 200, (int) color.blue() + 1, 300, 0, 0, 0, 255 );
			boxRGBA( screen, (int) color.alpha() - 1, 300, (int) color.alpha() + 1, 400, 0, 0, 0, 255 );
			SDL_Flip( colorSurface );
	
			while (1){
				while ( SDL_PollEvent( &event ) ){
					if ( event.type == SDL_QUIT )
						closeProgram( screen, toolbar );
					if ( event.type == SDL_KEYDOWN ){
						screen = SDL_SetVideoMode( x, y, 32, flags );
						loadFromBackup( screen, TEMPADDRESS );
						SDL_FreeSurface( colorSurface );
						return;
					}
					if ( event.type == SDL_MOUSEBUTTONUP )
						leftButtonHold = false;
					if ( event.type == SDL_MOUSEBUTTONDOWN ){
						leftButtonHold = true;
						if ( event.button.y <= 100 )
							color.setRed( event.button.x );
						else if ( event.button.y <= 200 )
							color.setGreen( event.button.x );
						else if ( event.button.y <= 300 )
							color.setBlue( event.button.x );
						else if ( event.button.y <= 400 )
							color.setAlpha( event.button.x );

						boxRGBA( colorSurface, 0, 0, 255, 100, 255, 0, 0, 255 );
						boxRGBA( colorSurface, 0, 100, 255, 200, 0, 255, 0, 255 );
						boxRGBA( colorSurface, 0, 200, 255, 300, 0, 0, 255, 255 );
						boxRGBA( colorSurface, 0, 300, 255, 400, 255, 255, 255, 255 );


						boxRGBA( screen, (int) color.red() - 1, 0, (int) color.red() + 1, 100, 0, 0, 0, 255 );
						boxRGBA( screen, (int) color.green() - 1, 100, (int) color.green() + 1, 200, 0, 0, 0, 255 );
						boxRGBA( screen, (int) color.blue() - 1, 200, (int) color.blue() + 1, 300, 0, 0, 0, 255 );
						boxRGBA( screen, (int) color.alpha() - 1, 300, (int) color.alpha() + 1, 400, 0, 0, 0, 255 );

						boxRGBA( colorSurface, 300, 0, 500, 400, 0, 0, 0, 255 );
						boxRGBA( colorSurface, 300, 0, 500, 400, color.red(), color.green(), color.blue(), color.alpha() );
						SDL_Flip( colorSurface );
					}
					if ( event.type == SDL_MOUSEMOTION )
						if (leftButtonHold){
							if ( event.motion.y <= 100 )
								color.setRed( event.motion.x );
							else if (event.motion.y <= 200 )
								color.setGreen( event.motion.x );
							else if (event.motion.y <= 300 )
								color.setBlue( event.motion.x );
							else if ( event.motion.y <= 400 )
								color.setAlpha( event.motion.x );

							cout << (int) color.alpha() << endl;

							boxRGBA( colorSurface, 0, 0, 255, 100, 255, 0, 0, 255 );
							boxRGBA( colorSurface, 0, 100, 255, 200, 0, 255, 0, 255 );
							boxRGBA( colorSurface, 0, 200, 255, 300, 0, 0, 255, 255 );
							boxRGBA( colorSurface, 0, 300, 255, 400, 255, 255, 255, 255 );


							boxRGBA( screen, (int) color.red() - 1, 0, (int) color.red() + 1, 100, 0, 0, 0, 255 );
							boxRGBA( screen, (int) color.green() - 1, 100, (int) color.green() + 1, 200, 0, 0, 0, 255 );
							boxRGBA( screen, (int) color.blue() - 1, 200, (int) color.blue() + 1, 300, 0, 0, 0, 255 );
							boxRGBA( screen, (int) color.alpha() - 1, 300, (int) color.alpha() + 1, 400, 0, 0, 0, 255 );



							boxRGBA( colorSurface, 300, 0, 500, 400, 0, 0, 0, 255 );
							boxRGBA( colorSurface, 300, 0, 500, 400, color.red(), color.green(), color.blue(), color.alpha() );

							SDL_Flip( colorSurface );
						}
				}
			}
		}
		else if ( event.key.keysym.sym == SDLK_c ){
			getBackup( screen, TEMPADDRESS );
			drawColorCircle( screen );
			while ( 1 ){
				while ( SDL_PollEvent( &event ) ){
					if (event.type == SDL_QUIT)
						closeProgram( screen, toolbar );
					if ( event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE ){
						loadFromBackup( screen, TEMPADDRESS );
						return;
					}
					if ( event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT ){
						Uint32 *pixel = (Uint32*) screen->pixels;
						Uint8 *colors = (Uint8*) &( pixel[ event.button.y * screen->w + event.button.x ] );

						color.setRed( (int) colors[2] );
						color.setGreen( (int) colors[1] );
						color.setBlue( (int) colors[0] );

					}
				}
			}
		}
	}
}

void checkMenu( SDL_Event event, Menu *menu, Toolbar &toolbar ){
	if ( event.type == SDL_KEYDOWN )
		if ( event.key.keysym.sym == menu->key() )
			menu->setupMenu( toolbar );
}

void checkHelp( SDL_Event event, Helper *helper, Toolbar &toolbar ){
	if ( event.type == SDL_KEYDOWN )
		if ( event.key.keysym.sym == helper->key() )
			helper->showHelp( toolbar );
}

void checkOpener( SDL_Event event, SDL_Surface* screen, Opener *opener, int &counter ){
	if ( event.type == SDL_KEYDOWN )
		if ( event.key.keysym.sym == opener->key() )
			if ( event.key.keysym.sym != 0 && ( event.key.keysym.mod & KMOD_CTRL ) ){
				opener->browse( screen );
				getBackup( screen, BACKUPFOLDER, &counter );
			}
}

void checkSaveRequest( SDL_Event event, SDL_Surface *screen, Saver *saver ){
	if (event.type == SDL_KEYDOWN)
		if ( event.key.keysym.sym == saver->key() )
			if ( event.key.keysym.mod != 0 && (event.key.keysym.mod & KMOD_CTRL ) )
				saver->saveImage( screen );
}

void checkUndo( SDL_Event event, SDL_Surface *screen, int &counter ){
	if (event.type == SDL_KEYDOWN)
		if (event.key.keysym.sym == SDLK_z)
			if (event.key.keysym.mod != 0 && ( event.key.keysym.mod & KMOD_CTRL ) ){
				char c[2000];
				sprintf( c, "%s%s%d", BACKUPFOLDER, "/screen", counter = max( counter - 1, 0 ) );
				loadFromBackup( screen, c );
			}
}

void checkRedo( SDL_Event event, SDL_Surface *screen, int &counter ){
}

void checkShiftState( SDL_Event event, bool &shift ){
	if ( event.type == SDL_KEYDOWN )
		if ( event.key.keysym.sym == SDLK_LSHIFT || event.key.keysym.sym == SDLK_RSHIFT )
			shift = true;
	if ( event.type == SDL_KEYUP )
		if ( event.key.keysym.sym == SDLK_LSHIFT || event.key.keysym.sym == SDLK_RSHIFT )
			shift = false;
}

int main(){

//	Initializing
	SDL_Init( SDL_INIT_EVERYTHING );
	srand( time( 0 ) );

//	Making Backups Folder
	system( "cd /tmp && mkdir paint && cd paint && mkdir paint-backup" );

//	Setting Tools Up
	Pen *pen = new Pen( true, SDLK_b );
	Rectangle *rectangle = new Rectangle( false, SDLK_r );
	Oval *oval = new Oval( false, SDLK_o );
	Polygon *polygon = new Polygon( false, SDLK_p, Cor( 0, 0 ) );
	Triangle *triangle = new Triangle( false, SDLK_t );
	Line *line = new Line( false, SDLK_l );
	Spray *spray = new Spray( false, SDLK_s );
	Filler *filler = new Filler( false, SDLK_f );
	Bucket *bucket = new Bucket( false, SDLK_d );
	Eye *eye = new Eye( false, SDLK_e );

	Menu *menu = new Menu( SDLK_m );
	Saver *saver = new Saver( SDLK_s );
	Helper *helper = new Helper( SDLK_h );
	Opener *opener = new Opener( SDLK_o );

//	Setting Toolbar Up
	Toolbar toolbar;
	setUpToolbar( toolbar, pen, rectangle, oval, polygon, triangle, line, spray, filler, bucket, eye );

//	Setting screen up
	const SDL_VideoInfo *info = SDL_GetVideoInfo();
	SDL_Surface *screen = SDL_SetVideoMode( info->current_w / 2, info->current_h / 2, 32, SDL_HWSURFACE | SDL_DOUBLEBUF );
	char caption[200];
	sprintf( caption, "UPaint! - %c: Menu, %c: Help", menu->key(), helper->key() );
	SDL_WM_SetCaption( caption, NULL );

//	variables
	SDL_Event event;
	int x, y;
	bool leftButtonHold = false, rightButtonHold = false, shiftDown = false;
	int radius = 5, backupsCounter = 0;
	Color color( 0, 0, 0, 255 );

	Color *fillColor = new Color( 255, 255, 255, 255 );
	toolbar[FILLER]->draw( screen, 0, 0, 0, 0, 0, *fillColor );
	SDL_Flip( screen );
	delete fillColor;

	char c[2000]; sprintf( c, "%s%s%d", BACKUPFOLDER, "/screen", backupsCounter );
	SDL_SaveBMP( screen, c );

	while ( 1 ){
		while (SDL_PollEvent (&event) ){
			
//			Setting Cordinate
			if (event.type == SDL_MOUSEBUTTONDOWN)
				setCordinate( x, y, event.button.x, event.button.y );

//			cheking quit
			checkQuit( event, screen, toolbar );

//			checking color
			checkColor( event, screen, color, leftButtonHold, toolbar );

//			checking menu
			checkMenu( event, menu, toolbar );

//			checking help
			checkHelp( event, helper, toolbar );
			
//			checking file open
			checkOpener( event, screen, opener, backupsCounter );

//			checking radius value
			checkRadius( event, radius );

//			checking if left mouse button is hold
			checkMouseButtonHold( event, leftButtonHold, rightButtonHold, x, y );

//			checking if shift button is down
			checkShiftState( event, shiftDown );

//			checking Tool Selecting
			checkToolSelection( event, toolbar );

//			checking Tools
			checkDrawing( event, screen, x, y, toolbar, leftButtonHold, rightButtonHold, shiftDown, radius, color, backupsCounter );

//			checking for save requsts
			checkSaveRequest( event, screen, saver );

//			checking for undoes
			checkUndo( event, screen, backupsCounter );

		}
	}

	closeProgram(screen, toolbar);
}

