
#ifndef CROP_H
#define CROP_H

#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

class Crop : public Tool {
	public:
		Crop( bool state, SDLKey key ) : Tool( state, key ) {}

		virtual void draw( SDL_Surface *screen, int, int, int, int, int, Color );
};

#endif

