
#include <iostream>

#include "pen.h"

void Pen::draw( SDL_Surface* screen, int x1, int y1, int x2, int y2, int radius, Color &color ){
/*	filledEllipseRGBA( screen, x, y, radius, radius, 255, 255, 255, 255 );
	filledEllipseRGBA( screen, newX, newY, radius, radius, 255, 255, 255, 255 );*/
/*	double length = sqrt( pow( newX - x , 2 ) + pow( newY - y, 2 ) );
	double theta = acos( ((newX - x) / length) * (M_PI / 180) );
	if ( newY - y > 0 ){
		theta += M_PI;
		std::cout << "Dn!\n";
	}
	short int vx[4], vy[4];
	double alpha = theta + ( 90 * (M_PI / 180) );
	vx[0] = x + (radius * cos( alpha ));
	vy[0] = y + (radius * sin( alpha ));
	alpha = theta - ( 90 * M_PI / 180 );
	vx[1] = x + (radius * cos( alpha ));
	vy[1] = y + (radius * sin( alpha ));
	vx[2] = vx[0] + ( ( newX - x ) * cos( alpha ) );
	vx[3] = vx[1] + ( ( newX - x ) * cos( alpha ) );
	vy[2] = vy[0] + ( ( newY - y ) * sin( alpha ) );
	vy[3] = vy[1] + ( ( newY - y ) * sin( alpha ) );
	for (int i = 0; i < 4; i++)
		std::cout << "x:" << vx[i] << "   y:" << vy[i] << std::endl;
	std::cout << std::endl;
	filledPolygonRGBA( screen, vx, vy, 4, 255, 255, 255, 255 );*/
	if (x1 == x2 && y1 == y2){
		filledEllipseRGBA( screen, x1, y1, radius / 2, radius / 2, color.red(), color.green(), color.blue(), color.alpha() );
		return;
	}
	
	int hyp = sqrt( pow( x2 - x1, 2 ) + pow( y2 - y1, 2) );
	double alpha;
	double sinos = asin( (y1 - y2) / hyp );
	double cosin = acos( (x1 - x2) / hyp );
/*	if (sinos >= 0 && cosin >= 0)
		alpha = asin( sinos );
	if (sinos >= 0 && cosin <= 0)
		alpha = asin( sinos ) + (M_PI / 2 );
	if (sinos <= 0 && cosin <= 0)
		alpha = acos( cosin );
	if (sinos <= 0 && cosin >= 0)
		alpha = acos( cosin ) + (M_PI / 2 );*/

	short int vx[4], vy[4];

	vx[0] = x1 - ( sin( alpha ) * radius / 2 );
	vy[0] = y1 - ( cos( alpha ) * radius / 2 );

	vx[1] = x1 + ( sin( alpha ) * radius / 2 );
	vy[1] = y1 + ( cos( alpha ) * radius / 2 );

	vx[2] = x2 + ( sin( alpha ) * radius / 2 );
	vy[2] = y2 + ( cos( alpha ) * radius / 2 );

	vx[3] = x2 - ( sin( alpha ) * radius / 2 );
	vy[3] = y2 - ( cos( alpha ) * radius / 2 );

	filledPolygonRGBA( screen, vx, vy, 4, color.red(), color.green(), color.blue(), color.alpha() );

//	lineRGBA( screen, x, y, newX, newY, color.red(), color.green(), color.blue(), color.alpha() );
}
