
#ifndef FILLER_H
#define FILLER_H

#include "tool.h"

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

class Filler : public Tool{
	public:
		Filler( bool state, SDLKey key ) : Tool( state, key, "Filler" ) {}

		virtual void draw( SDL_Surface*, int, int, int, int, int, Color &color ) override;
};

#endif

